# SEOSAW - A Socio-Ecological Observatory for Southern African Woodlands

This repository holds data and code related to the SEOSAW project. Here is a breakdown of the repository structure

* `cleaning` - Site level R scripts to transform datasets into SEOSAW-formatted data.
* `seosawr` - The SEOSAW R package, containing functions used to clean and manipulate SEOSAW-formatted data
* `doc` - Manuals, resources, code and outputs for the SEOSAW website

Note that the SEOSAW dataset resides in a different repository, with more limited access.

## How do I get set up?

In a UNIX terminal, `git clone` this repository, or alternatively download it as a zip file:

```
# Using SSH
git clone git@bitbucket.org:miombo/seosaw.git
```

To install the latest release of the SEOSAW R package `{seosawr}`, open R, then install the `{remotes}` package:

```r
install.packages("remotes")
```

Then install the package:

```r
remotes::install_bitbucket("miombo/seosaw/seosawr/seosawr")
```

The package can then be loaded in R:

```r
library(seosawr)
```

The `{BIOMASS}` package is a dependency for `{seosawr}`, however, this package is not currently available through CRAN (September 2023). You may have to install this package from their Github repository before you are able to successfully install `{seosawr}`:

```r
# install.packages("remotes")
remotes::install_github("umr-ama/BIOMASS")
```

## Repo etiquette

* Try to follow the guidelines here: [Good Enough Practices for Scientific Computing](https://swcarpentry.github.io/good-enough-practices-in-scientific-computing/)
* Place thorough explanatory comments at the start of every script, no matter how basic the code. 
* Aim to make all programs automated and free of errors, to increase reproducibility of research.
* Refrain from running `git add -A`. Only stage and commit source files, not intermediate files or large outputs.
* Do not commit SEOSAW data to this repository. The SEOSAW data is sensitive and should be kept in a separate repository.

## SEOSAW R package

The SEOSAW R package provides functions and example workflows for creating and cleaning SEOSAW-like data, and for analysing the existing SEOSAW dataset.

## Contact details

Please contact the repository owner: Dr. Casey M. Ryan - casey.ryan@ed.ac.uk
