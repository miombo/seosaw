# stem_status

* "a" - alive, signs of life above the DBH measurement.
* "r" - resprouting, stem is dead above DBH measurement, but signs of life elsewhere on the tree, either on same stem, at base, or on other stems.
* "d" - dead, no sign of life anywhere on the tree, all other stems dead, no leaves.

# stem_mode 

* "b" - broken (material above break is detached),
* "c" - has climbing or strangling plants in canopy,
* "f" - fallen,
* "g" - hollow,
* "j" - bark peeling or lost,
* "k" - stem has bracket fungi,
* "l" - leaning (>15deg),
* "m" - on a termite mound,
* "n" - a new recruit,
* "p" - snapped (material above break is attached),
* "q" - the stem location is unknown and was not found (contrast with "v"),
* "s" - standing,
* "t" - stump (broken below POM)
* "u" - uprooted,
* "v" - vanished (location of tree found but stem missing),
* "w" - stem is a liana,
* "y" - wounded,
* "z" - sick or rotten wood (i.e. lost most leaves, discoloured foliage, drooping branches)

# damage_cause 

* "a" - animals (not elephants, not insects)
* "e" - elephants
* "f" - fire
* "h" - humans (.e.g. cut, ringbarked, broken or beehive)
* "i" - frost
* "l" - lightning
* "m" - termites
* "n" - neighbouring tree
* "o" - other (describe in damage_cause_notes)
* "q" - unknown
* "w" - wind

