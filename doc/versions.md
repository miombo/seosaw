# SEOSAW versions

## SEOSAW database

* 2021-02-17 - v2.11
* 2021-03-13 - v2.12
* 2022-10-14 - v3.0
* 2023-03-11 - v3.1
* 2024-10-25 - v3.2

## Field manual

* 2021-02-17 - v3.2
* 2021-03-13 - v3.3
* 2021-10-20 - v3.4
* 2022-10-15 - v3.6

## Small stems manual

* 2022-06-24 - v1.0

## Dataset manual

* 2021-02-17 - v2.11
* 2021-03-13 - v2.12
* 2022-10-15 - v3.0

## Ground layer manual 

* 2021-02-17 - v0.1
* 2022-01-11 - v1.0
* 2022-06-24 - v1.4

## Traits manual

* 2021-02-17 - v1.0

## Coarse woody debris manual

* 2022-06-24 - v1.0

## Plot field datasheet

* 2021-02-17 - v1.1
* 2021-03-13 - v1.2
* 2021-09-21 - v1.3
* 2021-09-23 - v1.4

## Stem field datasheet

* 2021-02-17 - v1.1
* 2021-03-13 - v1.2
* 2021-09-21 - v1.4
* 2021-09-23 - v1.5
* 2022-08-30 - v1.6

## Disturbance field datasheet

* 2021-09-21 - v1.3

## ODK plots form

* 2021-02-17 - 20210114
* 2021-03-13 - 20210312
* 2022-09-07 - 20220907

## ODK stems form

* 2021-02-17 - 20210114
* 2021-03-13 - 20210312
* 2022-08-30 - 20220830

## ODK small stems form

* 2022-08-30 - 20220830

## ODK ground layer form

* 2022-08-30 - 20220830

## ODK disturbance form 

* 2022-10-14 - 20220917

## Code of Conduct

* 2021-02-17 - v2.1

## Code of conduct plain language summary

* 2021-02-17 - v2.1

## Soils manual

* 2021-09-21 - v1.4
* 2022-06-24 - v1.7

## Disturbance field datasheet

* 2021-09-21 - v1.3
* 2022-05-09 - v1.4
* 2022-07-11 _ v1.5

## Field cheat sheet

* 2021-09-21 - v1.0
* 2021-09-23 - v1.1
* 2022-02-07 - v1.2
* 2022-08-30 - v1.3

