# Subset some plots from the most recent database version to use as examples online
# Lorena Benitez, John L. Godlee (johngodlee@gmail.com)
# Last updated: 2023-03-11

# Packages 
library(dplyr)

# Define data path
ver_id <- "v3.1" 
out_dir <- file.path("~/git_proj/seosaw_data/data_out", ver_id)

# Load data
s <- read.csv(file.path(out_dir, paste0("stems_", ver_id, ".csv")))
p <- read.csv(file.path(out_dir, paste0("plots_", ver_id, ".csv")))
 
# Get plot data
p_aces <- p %>% 
  filter(dataset_id == "MAR")

p_nhambita <- p %>% 
  filter(plot_id %in% c("MGR_13","MGR_14","MGR_15")) %>%
  dplyr::select(
    -longitude_of_centre,
    -latitude_of_centre)

# Get stem data
s_nhambita <- s %>% 
  filter(plot_id %in% c("MGR_13","MGR_14","MGR_15")) %>%
  filter(stem_id %in% sample(stem_id, 1000))

s_aces <- s %>% 
  filter(plot_id %in% p_aces$plot_id)

# Write to file
write.csv(p_aces, "single_census_plot.csv", row.names = FALSE)
write.csv(p_nhambita, "multi_census_plot.csv", row.names = FALSE)
write.csv(s_aces, "single_census_stems.csv", row.names = FALSE)
write.csv(s_nhambita, "multi_census_stems.csv", row.names = FALSE)
