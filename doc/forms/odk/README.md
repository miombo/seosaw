# ODK forms for SEOSAW plot setup

There are two principle forms available, with auxiliary media that are called by each:

* `plot.xml`
	* `nested.png`
	* `subplot.png`
* `stems_species_dynamic.xml`
	* `loc.png`
	* `nested_subplot.png`

There are also other forms which are variations on these two main forms.

For local testing, install "ODK Collect" on an Android device from the Google Play Store. 

Transfer the `.xml` forms to the Android device. Place them in `/odk/forms/`. Transfer the auxiliary files to `/odk/forms/plot-media/` and `/odk/forms/stems-media/`, respectively. On macOS use [Android File Transfer](https://www.android.com/filetransfer/) to transfer files.

Open ODK Collect on the Android device.

Select "Fill Blank Form". Both forms should be visible. Choose either one of the forms to start entering data.

Some features of the forms:

* Sanity checks on most numerical inputs. For example DBH must be within 0 and 1000 cm and within plot-level DBH thresholds if they are set.
* Option to capture plot corners, plot centre, stem locations with GPS in the Android device.
* Species list dynamically generates from previously submitted species names.
* Nearly every response provided maps to a column in the plot/stems tables in the SEOSAW dataset.
* The stems form encourages nesting stems within trees and allows for an unlimited number of repeated measurements within a single plot.

`forms_compile.sh` compiles all the forms and their media.

`country_list_gen.R` generates a list of all the countries in Africa which is then sent to the plots form

