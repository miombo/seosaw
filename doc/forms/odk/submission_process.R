# Process CSV (legacy) output from SEOSAW plot and stem data collection forms
# John Godlee (johngodlee@gmail.com)
# 2021-01-15
#Revised by Lorena Benitez on 27/01/2022

# Use XLS (legacy) output from Kobotoolbox

# Command line arguments
if (!interactive()) {
  args <- lapply(commandArgs(trailingOnly = TRUE), function(x) { 
    sub("\\/$", "", x) })

  if (length(args) != 2) {
    stop("Arguments must be supplied:\n[1] Plots .xlsx file\n[2] Stems .xlsx file\n")
  }
}

# Packages
library(dplyr)
library(readxl)
library(sf)
library(seosawr)

# File names
plots_file <- args[1]
stems_file <- args[2]

# Import data sheet by sheet
# Stems
stems_plot <- read_excel(stems_file, sheet = 1)
stems_tree <- read_excel(stems_file, sheet = "tree_repeat")
stems_stem <- read_excel(stems_file, 
  sheet = "tree_repeat_stem_repeat")
stems_imag  <- read_excel(stems_file, 
  sheet = "tree_repeat_species_image_repea")

# Plots 
plots <- read_excel(plots_file, sheet = 1)
plots_rec_corner_gps <- read_excel(plots_file, 
  sheet = "loc_group_rectangle_corner_repe")
plots_irr_corner_gps <- read_excel(plots_file, 
  sheet = "loc_group_irregular_corner_repe")
plots_imag <- read_excel(plots_file,
  sheet = "image_repeat")

# Clean stems_plot columns
stems_plot_clean <- stems_plot %>%
  rename_with(~gsub("plot_group\\/", "", .x)) %>%
  dplyr::select(
    plot_name,
    measurement_date,
    diam_method,
    diam_method_manual,
    height_method,
    height_method_manual,
    xy_method,
    xy_latlong_method,
    `_index`) %>%
  mutate(plot_id = `_index`,
         measurement_date = as.Date(measurement_date),
         diam_method = case_when(
           !is.na(diam_method_manual) ~ as.character(diam_method_manual),
           TRUE ~ diam_method),
         height_method = case_when(
           !is.na(height_method_manual) ~ as.character(height_method_manual),
           TRUE ~ height_method),
         xy_method = case_when(
           !is.na(xy_latlong_method) ~ as.character(xy_latlong_method),
           TRUE ~ xy_method))

# Clean stems_tree columns
stems_tree_clean <- stems_tree %>%
  mutate(    plot_id = `_parent_index`,
             tree_id = `_index`,
             species = case_when(
               !is.na(species_manual) ~ as.character(species_manual),
               TRUE ~ as.character(sp)),
             longitude = case_when(
               !is.na(`_stem_loc_longitude`) ~ as.numeric(`_stem_loc_longitude`),
               !is.na(longitude) ~ as.numeric(longitude),
               TRUE ~ NA_real_),
             latitude = case_when(
               !is.na(`_stem_loc_latitude`) ~ as.numeric(`_stem_loc_latitude`),
               !is.na(latitude) ~ as.numeric(latitude),
               TRUE ~ NA_real_)) %>%
  rename(
    elevation = `_stem_loc_altitude`,
    gps_precision = `_stem_loc_precision`) %>%
  dplyr::select(
    -`_stem_loc_latitude`,
    -`_stem_loc_longitude`,
    -sp)%>%
  rename_with(~gsub(".*\\/", "", .x))

# Clean stems_stem columns
stems_stem_clean <- stems_stem  %>%
  mutate(diam_caliper_mean = mean(c(diam_caliper_1, diam_caliper_2),
                                  na.rm = TRUE),
         diam=as.numeric(stems_stem$diam)) %>%
  ungroup() %>%
  mutate(    tree_id = `_parent_index`,
             stem_id = `_index`,
             diam = case_when(
               !is.na(diam_caliper_mean) ~ diam_caliper_mean,
               !is.na(circumference) ~ circumference / pi,
               !is.na(diam) ~ diam,
               TRUE ~ NA_real_),
             damage_human = case_when(
               !is.na(damage_human_manual) ~ as.character(damage_human_manual),
               TRUE ~ as.character(damage_human)),
             other = case_when(
               !is.na(damage_manual) ~ as.character(damage_manual),
               TRUE ~ as.character(other))) %>%
  dplyr::select(
    -circumference,
    -diam_caliper_1,
    -diam_caliper_2,
    -diam_caliper_mean,
    -mortality_boolean_group,
    -damage_boolean_group,
    -damage_human_manual,
    -damage_manual)%>%
  rename_with(~gsub(".*\\/", "", .x)) %>%
  rowwise()

# Clean stems_imag columns
stems_imag_clean <- stems_imag %>%
  dplyr::select(
    img_file = "tree_repeat/species_image_repeat/species_image",
    tree_id = `_parent_index`) %>%
  group_by(tree_id) %>%
  summarise(img_file = paste(img_file, collapse = ", "))

# Merge tree data into stems by tree_id
stems_join <- left_join(stems_stem_clean, stems_tree_clean, by = "tree_id") %>%
  left_join(., stems_imag_clean, by = "tree_id")  %>%
  left_join(., stems_plot_clean, by = "plot_id")

# Clean plots data 

# # Select columns and rename to drop group before leading slash
# plots_sel <- plots %>%
#   dplyr::select(
#     contains("basic_group"), 
#     contains("loc_group"),
#     contains("herbivory_group"),
#     contains("sampling_group"),
#     contains("land_use_group"),
#     contains("fire_group"),
#     plot_id = `_index`) %>%
#   rename_with(~gsub(".*\\/", "", .x))

# Remove some uninformative columns
# If use code commented above change to plots_sel
plots_clean <- plots %>%
  dplyr::select(
    -date,
    -subplots,
    -loc_choice,
    -plot_corners,
    -plot_center,
    -herbivory_boolean_group_labels,
    -sampling_boolean_group_labels,
    -land_use_boolean_group_labels)%>%
  mutate(plot_id = `_index`,
         nested_1 = case_when(
           nested %in% c("1", "2", "3") ~ TRUE,
           TRUE ~ FALSE),
         nested_2 = case_when(
           nested %in% c("2", "3") ~ TRUE,
           TRUE ~ FALSE),
         nested_3 = case_when(
           nested %in% c("3") ~ TRUE,
           TRUE ~ FALSE),
         tree_diff = TRUE) %>%
  select( -starts_with("_"))%>%
  left_join(., stems_plot_clean[,c("plot_id", "xy_method")],
            by = "plot_id")

# Plot polygons

# Extract coordinate data from plots table
plots_coords <- plots %>%
  mutate(plot_id = `_index`)%>%
  dplyr::select(
    plot_id,
    plot_name,
    plot_shape,
    plot_length,
    plot_width,
    plot_corners,
    `_plot_center_latitude`,
    `_plot_center_longitude`,
    `_plot_center_altitude`,
    `_plot_center_precision`,
    circle_long,
    circle_lat)

# Corners added by phone GPS
plots_coords_list <- strsplit(plots_coords$plot_corners, ";")
names(plots_coords_list) <- plots_coords$plot_id

plots_coords_df_list <- lapply(seq(length(plots_coords_list)), function(x) {
  coords_split <- strsplit(plots_coords_list[[x]], " ")
  out <- as.data.frame(do.call(rbind, coords_split))
  out$plot_id <- names(plots_coords_list[x])
  if (ncol(out) == 5) { 
    names(out) <- c("latitude", "longitude", "elevation", "precision", "plot_id")
    out <- out[,c(5,1,2,3,4)]
  } else {
    out <- NA
  }
  return(out)
})

polys <- do.call(rbind, lapply(plots_coords_df_list, function(x) {
  if (!is.na(x)) {
    return(st_sf(plot_id = unique(x$plot_id), 
        geometry = st_cast(st_combine(st_as_sf(x, 
          coords = c("longitude", "latitude"))), "POLYGON")))
  } 
}))

st_crs(polys) <- 4326

# Corners added by handheld GPS
manual_corners_list <- list(plots_rec_corner_gps, plots_irr_corner_gps)

manual_corners_points <- do.call(rbind, 
  lapply(manual_corners_list, function(x) {
    x_clean <- x %>%
      rename_with(~gsub(".*\\/", "", .x)) %>%
      dplyr::select(ends_with("_long"), ends_with("_lat"), 
        plot_id = `_parent_index`)
    out <- st_as_sf(x_clean,
      coords = c(1, 2)) 
}))

manual_corners_polys <- st_sf(
  aggregate(manual_corners_points$geometry,
    list(plot_id = manual_corners_points$plot_id),
    function(x) {
      st_cast(st_combine(x), "POLYGON")
    }
   ))

st_crs(manual_corners_polys) <- 4326

# Generate polygons for circles from handheld GPS center point
circle_polys <- plots_coords %>%
  filter(
    plot_shape == "circle",
    !is.na(circle_long), 
    !is.na(circle_lat)) %>%
  mutate(polygons = FALSE) %>%
  polyGen(., longitude_of_centre = "circle_long", 
    latitude_of_centre = "circle_lat")

# Generate polygons for circles from phone GPS center point
circle_manual_polys <- plots_coords %>%
  filter(
    plot_shape == "circle",
    !is.na(`_plot_center_latitude`),
    !is.na(`_plot_center_longitude`)) %>%
  mutate(polygons = FALSE) %>%
  polyGen(., longitude_of_centre = "_plot_center_longitude", 
    latitude_of_centre = "_plot_center_latitude")

# Combine all polygon types
polys_all <- rbind(polys, manual_corners_polys, circle_polys, 
  circle_manual_polys)

# Which plots have polygons?
plots_clean$polygons <- case_when(
  plots_clean$plot_id %in% polys_all$plot_id ~ TRUE,
  TRUE ~ FALSE)

# Add plot images to plots dataframe
plots_imag_clean <- plots_imag %>%
  dplyr::select(
    plot_id = `_parent_index`,
    img_file = `image_repeat/plot_image`) %>%
  group_by(plot_id) %>%
  summarise(img_file = paste(img_file, collapse = ", "))

plots_clean_imag <- plots_clean %>%
  left_join(., plots_imag_clean, by = "plot_id")

# Export data
write.csv(plots_clean_imag, "~/Downloads/plots_clean.csv", row.names = FALSE)
write.csv(stems_join, "~/Downloads/stems_clean.csv", row.names = FALSE)
st_write(polys_all, "~/Downloads/polys.shp")



