This is a variant of the stem data entry form built to re-census an existing plot.

This form requires an external `stems.csv` file containing stem measurements from the most recent previous census of a single plot. `stems.csv` should contain these columns at minimum, even if all records are NA in the original data: 

* `tag_id`
* `census_date`
* `species`
* `diam`
* `pom`
* `height`
* `bole_height`
* `alive`
* `stem_status`
* `stem_mode`
* `standing_fallen`
* `x_grid`
* `y_grid`
* `longitude`
* `latitude`
* `angle`
* `distance`
* `notes_stem`
* `on_termites`
* `liana`

`stems.csv` can contain extra columns, but these cannot be accessed within the form.

This form must be deployed separately for each plot you wish to recensus. `stems.csv` must only contain records for a single plot.

