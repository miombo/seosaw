This is a variant of the stem data entry form which uses the entirety of the [CJB African plants database](https://www.ville-ge.ch/musinfo/bd/cjb/africa/recherche.php) to choose species names rather than using the dynamically populating list method in the main stem data entry form. 

When processed, this form produces an external `itemsets.csv` which should be added to the media directory for the form on the Android device. 

All the code used to process the raw CJB species list, is found in `cjb_compile/`.

Note that for this form to deploy with KoboToolbox, the XLSForm must specify `label::English (en)` rather than simply `label`. This is a bug in KoboToolbox.

