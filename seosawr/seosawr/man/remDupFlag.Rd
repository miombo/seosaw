% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{remDupFlag}
\alias{remDupFlag}
\title{Remove duplicated flag values within strings}
\usage{
remDupFlag(x)
}
\arguments{
\item{x}{vector of character strings}
}
\value{
vector of character strings with duplicated values removed
}
\description{
Remove duplicated flag values within strings
}
\examples{
remDupFlag(c("FU", "FUFF", "FJJ"))


}
