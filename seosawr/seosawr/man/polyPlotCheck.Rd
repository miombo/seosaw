% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/spatial.R
\name{polyPlotCheck}
\alias{polyPlotCheck}
\title{Check if areas and distance between centers is similar between plot data and polygons}
\usage{
polyPlotCheck(
  x,
  poly_data,
  plot_plot_id = "plot_id",
  poly_plot_id = plot_plot_id,
  longitude_of_centre = "longitude_of_centre",
  latitude_of_centre = "latitude_of_centre",
  plot_area = "plot_area",
  dist_thresh = 10000,
  area_thresh = 1000,
  output = TRUE,
  crs = 4326
)
}
\arguments{
\item{x}{dataframe of plot measurements}

\item{poly_data}{sf object containing plot polygons}

\item{plot_plot_id}{column name of plot IDs in \code{plot_data}}

\item{poly_plot_id}{column name of plot IDs in \code{x}}

\item{longitude_of_centre}{column name of plot longitude in \code{plot_data}}

\item{latitude_of_centre}{column name of plot latitude in \code{plot_data}}

\item{plot_area}{column name of plot area in \code{plot_data}}

\item{dist_thresh}{threshold distance between plot centres, in metres}

\item{area_thresh}{threshold area difference, in $m^2$, between plot area in 
\code{x} and \code{poly_data}}
}
\value{
dataframe of measurement IDs with x_grid and y_grid filled 
    with estimated XY grid coordinates. NA values returned if 
    longitude and latitude were missing.

vector of row indices in \code{x} where values of 
    plot_area aren't similar to area of corresponding polygons in 
    \code{poly_data}.
}
\description{
Check if areas and distance between centers is similar between plot data and polygons
}
