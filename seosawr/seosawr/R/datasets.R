#' Polygon defining the extent of the SEOSAW region
#' 
#' An estimate of SEOSAW's operating region, based on the presence of woody 
#' savannas and woodlands in White's vegetation map. 
#'
#' A key issue was defining the north-eastern boundary of the region. 
#' It is defined here as the boundaries of Kenya and Uganda, excluding 
#' Ethiopia, Somalia, and South Sudan. While the decision to use country 
#' boundaries was largely pragmatic, it is backed up by environmental data. 
#' There is a mountainous region on the border between Kenya, Ethiopia and 
#' Somalia. The mountains cause quite a clear change in precipitation, 
#' probably due them blocking dry Arabian air moving further southwest.
#'
#' The north-western boundary largely follows the rift valley lakes, with 
#' everything to the west of them being excluded. Some areas to the east of 
#' the lakes are also excluded as they are classed by White as Congo Forest 
#' Transitions and Mosaics.
#'
#' The south-east of the region is further reduced from White's vegetation 
#' map according to the WWF Ecoregions. White classes some areas as woodland 
#' or forest in this area, but Casey considered to be too temperate, and thus 
#' outside the SEOSAW region.
#'
#' @name seosaw_region
#' @docType data
#' @author John L. Godlee
#' @usage data(seosaw_region)
"seosaw_region"

#' Polygon defining the bounding box of the SEOSAW region
#'
#' A generously size bounding box around the SEOSAW region, based on 
#' \code{data(seosaw_region)}
#' 
#' @name seosaw_region
#' @docType data
#' @author John L. Godlee
#' @usage data(seosaw_region)
"seosaw_bbox"

#' ISO3166 alpha-3 compliant country codes for continental Africa
#'
#' Vector object containing three-letter ISO3166 country codes for all 
#' countries in southern Africa. Taken from 
#' https://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_in_Africa 
#' and converted to ISO3166 alpha-3 codes with \code{countrycode::countrycode()}
#' 
#' @name africa_iso3 
#' @docType data
#' @author John L. Godlee
#' @usage data(africa_iso3)
"africa_iso3"

#' Country polygons for continental Africa
#'
#' List of countries taken from \code{data(africa_iso3)}. Spatial data from 
#' \code{rnaturalearth::ne_states()}
#'
#' @name africa
#' @docType data
#' @author John L. Godlee
#' @usage data(africa)
"africa"

#' Synonymy lookup table for Rhus species
#' 
#' Dataframe object containing original Rhus spp. and updated 
#'     Searsia species.
#'
#' @name rhus 
#' @docType data
#' @author John L. Godlee
#' @usage data(rhus)
"rhus"

#' Synonymy lookup table for ex-Acacia species, from Kyalangalilwa et al. (2013)
#' 
#' Dataframe object containing original Acacia/Mimosa spp. and updated 
#'     Senegalia/Vachellia species.
#'
#' @references Bruce Kyalangalilwa, James S. Boatwright, Barnabas H. Daru, 
#'     Olivier Maurin, Michelle van der Bank, Phylogenetic position and revised 
#'     classification of Acacia s.l. (Fabaceae: Mimosoideae) in Africa, 
#'     including new combinations in Vachellia and Senegalia, Botanical Journal 
#'     of the Linnean Society, Volume 172, Issue 4, August 2013, Pages 500–523, 
#'     https://doi.org/10.1111/boj.12047
#' 
#' @name acacia 
#' @docType data
#' @author John L. Godlee
#' @usage data(acacia)
"acacia"

#' Synonymy lookup table for Trillesanthus/Marquesia species
#' 
#' Dataframe object containing original Trillesanthus spp. and updated 
#'     Marquesia species.
#'
#' @name trillesanthus 
#' @docType data
#' @author John L. Godlee
#' @usage data(trillesanthus)
"trillesanthus"

#' Synonymy lookup table for Maytenus/Gymnosporia species
#' 
#' Dataframe object containing original Maytenus spp. and updated 
#'     Gymnosporia species.
#'
#' @name maytenus 
#' @docType data
#' @author John L. Godlee
#' @usage data(maytenus)
"maytenus"

#' Lookup table for SEOSAW species synonymy
#' 
#' Dataframe object containing various original species names and corrected names
#'
#' @name synonymy 
#' @docType data
#' @author John L. Godlee
#' @usage data(synonymy)
"synonymy"

#' Lookup table for CJB African Plant Database
#' 
#' Dataframe object of synonyms for species in the CJB African Plant Database.
#' Last updated: 2022-07-11
#' 
#' @name cjb_apd
#' @docType data
#' @author John L. Godlee, Cyrille Chatelain
#' @usage data(cjb_apd)
"cjb_apd"

#' Vector of species, genus indets, and family indets in SEOSAW dataset 
#' 
#' Vector of species names ("genus species"). 
#'     Generated using \code{seosawr:::seosawTaxaExport()}
#'
#' @name seosaw_taxa
#' @docType data
#' @author John L. Godlee
#' @usage data(seosaw_taxa)
"seosaw_taxa"

#' Example data
#' 
#' Example fictional dataset, containing plots, stems, CWD, small stems 
#'     and ground layer tables
#' 
#' @name ex_data 
#' @docType data
#' @author John L. Godlee
#' @usage data(ex_data)
"ex_data"

#' Dataframe of observed exotic species
#' 
#' Dataframe of exotic species observed in SEOSAW plots. The column 
#'     "naturalised" denotes whether the species is naturalised,
#'     according to the CJB African Plant Database. 
#' 
#' @name exotic 
#' @docType data
#' @author John L. Godlee
#' @usage data(exotic)
"exotic"
