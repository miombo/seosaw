#' Create empty columns based on a *ColClass function
#'
#' @param x dataframe containing measurements
#' @param colClass user-supplied list of column constructor functions
#'      named by column name. See \code{stemColClass()} and \code{plotColClass()} 
#' @return Dataframe with original and new columns filled with NAs
#' @export
#'
colGen <- function(x, colClass) {
  new_cols <- names(colClass)[!names(colClass) %in% names(x)]

  if (length(new_cols >= 1)) {
    message("New columns:\n  ",paste(new_cols, collapse = ",\n  "))
  }

  new_df <- as.data.frame(matrix(data = NA, nrow = nrow(x), ncol = length(new_cols)))
  names(new_df) <- new_cols
  new_df
  return(cbind(x, new_df))
}

#' Check if necessary column labels are present in stems dataframe
#'
#' @param x dataframe containing measurements
#' @param colClass user-supplied list of column constructor functions
#'     named by column name. See \code{stemColClass()} and \code{plotColClass()} 
#' 
#' @export
#'
colNameCheck <- function(x, colClass) {
  if(!all(names(colClass) %in% colnames(x))) {
    stop(paste("Columns not present:",
      paste(names(colClass)[!names(colClass) %in% colnames(x)], collapse = ", ")
    ))
  } else if (any(!colnames(x) %in% names(colClass))) {
    stop(paste("Extra columns present:",
      paste(colnames(x)[!colnames(x) %in% names(colClass)], collapse = ", ")
    ))
  } else {
    return("All columns present")
  }
}

#' Run column checking functions on corresponding named columns in a dataframe
#'
#' @param x dataframe containing measurements
#' @param colClass list of column constructor functions named by column name.
#'     \code{stemColClass()}, \code{plotColClass()}, or a similarly formatted 
#'     user supplied list.
#' 
#' @return Clean dataframe object
#'
#' @export
#'
colValCheck <- function(x, colClass) {
  colNameCheck(x, colClass = colClass)
  check_df <- data.frame(
    lapply(1:length(colClass), function(y) {
      colClass[[y]](x[[names(colClass[y])]], colname = names(colClass[y]))
    }))
  colnames(check_df) <- names(colClass)
  return(check_df)
}

#' Convert column names from SEOSAW v1 plot data tables to new names
#'
#' @param x dataframe of plot data from SEOSAW v1
#' @param rename_vec optional named vector of columns to keep (values) and 
#'     rename (names).
#'
#' @return dataframe with updated column names, some uninformative columns 
#'     removed.
#' 
#' @export
#' 
plotColNamesV1 <- function(x, rename_vec = NULL) {
  if (is.null(rename_vec)) { 
    rename_vec <- c(
      canopy_cover = "Canopy cover",
      canopy_cover_method = "Canopy cover method",
      catenal_position = "Catenal position",
      cattle_grazing = "Cattle graze here",
      census_date = "all dates",
      charcoal_harvesting = "Charcoal harvesting",
      country_iso3 = "Country",
      elephant_data_source = "Elephant data source",
      elephant_density = "Elephant density",
      exclosure = "Elephant exclosure",
      elephants = "Elephants",
      elevation = "Altitude",
      experiment_ref = "Experiment ref",
      farmed_30_years = "Used for farming in last 30 years?",
      fire_exclusion_success = "Protected from fire",
      fire_return_interval = "Treatment fire return interval",
      fire_treatment_season = "Treatment season",
      fuel_wood_harvesting = "Fuel wood harvesting",
      goat_grazing = "Goats graze here",
      ground_layer_sampled = "Understory sampled",
      high_graded_100_years = "Was the plot high graded in last 100 years",
      land_use_notes = "Land use history timeline",
      large_browsers = "Large browsers",
      latitude_of_centre = "Latitude of centre",
      lianas_sampled = "Lianas sampled",
      local_allometry = "Recommended allometry",
      local_community_rights = "Local community rights",
      longitude_of_centre = "Longitude of centre",
      manipulation_experiment = "Manipulation experiment?",
      medium_sized_social_mixed_diet = "Medium sized social mixed diet",
      n1_min_diam_thresh = "N1: Min tree Diameter",
      n1_min_height_thresh = "N1: Min tree Height",
      n2_min_diam_thresh = "N2: Min tree Diameter",
      n2_min_height_thresh = "N2: Min tree Height",
      n2_plot_length = "N2: Plot length",
      n2_plot_radius = "N2: Plot diameter",
      n2_plot_shape = "N2:shape",
      n2_plot_width = "N2: Plot width",
      n3_min_diam_thresh = "N3: Min tree Diameter",
      n3_min_height_thresh = "N3: Min tree Height",
      n3_plot_length = "N3: Plot length",
      n3_plot_shape = "N3:shape",
      n3_plot_width = "N3: Plot width",
      nested_1 = "Nested?",
      nonruminants_excluding_suids = "Nonruminants (excluding suids/pigs)",
      ntfp_harvesting = "NTFP harvesting",
      other_authors = "Other authors",
      other_user_rights = "Other users rights",
      other_woody_product_harvesting = "Other woody product harvesting",
      pa_type = "PA name and type",
      plot_id = "PlotCode",
      plot_length = "Plot Length",
      plot_name = "PlotName",
      plot_notes = "Plot comments",
      plot_radius = "Plot Diameter",
      plot_shape = "Shape of plot",
      plot_width = "Plot Width",
      polygons = "Polygon data exist?",
      prinv = "PI",
      sampling_design_code = "Sampling design: FP code",
      slope = "Average plot slope at 20m horizontal scale",
      small_nonsocial_browsers = "small nonsocial browsers",
      soil_sampled = "Soil sampled",
      stumps_sampled = "Stumps sampled",
      termites = "Termites",
      timber_harvesting = "Timber harvesting occurring", 
      wdpa_id = "PA status (WDPA ID)")
  }

  out <- x[,rename_vec]
  colnames(out) <- names(rename_vec)

  return(out)
}
