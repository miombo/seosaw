#' Check elements in multiple vectors are equal
#'
#' @param ... vectors of identical length
#'
#' @return vector of logical elements
#' 
#' @examples
#' a <- c(1,2,3)
#' b <- c(1,2,3)
#' c <- c(1,2,4)
#' allEqual(a,b,c)
#' @export
#' 
allEqual <- function(...) {
	input_list <- list(...)
	check.eq <- sapply(input_list[-1], function(x) {x == input_list[[1]]})
	all(check.eq == TRUE)
}

#' Extract the modal value of a vector
#'
#' @param x vector
#'
#' @return string of the most frequent value in x
#' 
#' @export
#' 
Mode <- function(x) {
  x <- x[!is.na(x)]
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

#' Catch non-standard values and return NAs
#'
#' @param x vector 
#' @param zero logical, should zeroes also be converted to NA? \code{FALSE} by default
#'
#' @return vector
#' 
#' @examples 
#' fillNA(c(1,2,3,Inf,NA,NaN,"nan","dinf","Inf","  ", " ff    "))
#' fillNA(c(0,1,2), zero = TRUE)
#'
#' @export
#'
fillNA <- function(x, zero = FALSE) {
  if (is.factor(x)) { x <- as.character(x) }
  out <- unlist(lapply(split(x, seq_along(x)), function(y) {
      # Inf, NaN, NA
      if (is.na(y) | is.nan(y) | is.infinite(y)) { 
        return(NA_character_) 
      # Character representations of NA, NaN, Inf,
      } else if (grepl("^n/a$|^na$|^nan$|^inf$", 
          trimws(iconv(y, "latin1", "ASCII", sub = " ")), 
          ignore.case = TRUE)) {
        return(NA_character_)
      # Empty character strings
      } else if (is.character(y) & nchar(trimws(y)) == 0) {
        return(NA_character_)
      } else {
        if (zero == TRUE) {
          if (y == 0) {
            return(NA)
          } else {
            return(y)
          }
        }
        return(y)
      }
      # Optionally change zeroes to NA
  }))
  # Change class to original
  out <- unname(as(out, class(x)))
  return(out)
}

#' Check if vector contains NA with message
#'
#' @param x vector 
#' @param warn logical; return warning instead of error
#' @param colname optional column name to display in error/warning 
#'
#' @return Vector
#' @keywords internal
#' @noRd
#'
naCatch <- function(x, warn = FALSE, colname) {
  if (is.null(colname)) {
    colname <- tryCatch( { 
      deparse(sys.calls()[[sys.nframe()-1]])
    },
      error=function() { 
        return(paste("")) 
      }
    )
  }
  if (anyNA(x)) {
    num <- length(x[is.na(x)])
    den <- length(x)
    output <- paste0(colname, ":", num, "/", den, " NAs")
    if (warn) {
      warning(output, call. = FALSE)
    } else {
      stop(output)
    }
  }
}

#' Check if vector contains negative values with message
#'
#' @param x vector 
#' @param warn logical; return warning instead of error
#' @param zero logical; if TRUE, also disallow zero
#' @param colname optional column name to display in error/warning 
#'
#' @return Vector
#' @keywords internal
#' @noRd
#'
negCatch <- function(x, warn = FALSE, zero = FALSE, colname) {
  if (is.null(colname)) {
    colname <- tryCatch( { 
      deparse(sys.calls()[[sys.nframe()-1]])
    },
      error=function() { 
        return(paste("")) 
      }
    )
  }

  if (zero) {
    testfunc <- `<=`
  } else { 
    testfunc <- `<`
  }

  x_nona <- x[!is.na(x)]
  if (any(testfunc(x_nona, 0))) {
    num <- length(which(testfunc(x_nona, 0)))
    den <- length(x)
    output <- paste0(colname, ":", num, "/", den, " negative values")
    if (warn) {
      warning(output)
    } else {
      stop(output)
    }
  }
}

#' Check for allowed values in a character vector
#'
#' @param x vector
#' @param val vector of allowed values
#' @param colname column name to display in error/warning 
#'
#' @return Vector 
#' @keywords internal
#' @noRd
#'
allowedValues <- function(x, val, colname) {
  if (!all(x %in% val)) {
    stop(paste("Illegal value in", colname, ":",
      paste(unique(x)[!unique(x) %in% val], collapse = ", "),
      "\n\t", "allowed levels:", paste(val, collapse = ", ")))
  }
  return(x)
}

#' Catch NA coercion errors
#'
#' @param x vector
#' @param fun coercion function, e.g. \code{as.integer}
#' @param colname column name to display in error/warning 
#' 
#' @return Vector coerced by fun 
#' @keywords internal
#' @noRd
#' 
coerceCatch <- function(x, fun, colname) { 
  tryCatch({
    fun(x)
  }, warning = function(w) {
    stop(paste("Unable to coerce column", colname, "to", deparse(substitute(fun)), ":", conditionMessage(w)))
  })
}

#' Format warnings, messages and errors
#'
#' @param x vector of row positions, IDs etc. to display
#' @param warn character string with warning, message or error
#' @param type choose either "warning", "message" or "error" 
#' @param n integer, number of items from x to return in warning
#' @keywords internal
#' @noRd
#' 
warnFormat <- function(x, warn, type, n = 10, sep = ", ") {
  if (length(x) == 0) {
    return(invisible())
  }

  nlen <- ifelse(length(x) > n, n, length(x))
  endv <- ifelse(length(x) > n, " ...", "")
  warn <- paste0(trimws(warn), " ")

  if (length(type) != 1 | !type %in% c("warning", "message", "error")) {
    stop("warn not 'warning', 'message' or 'error'")
  }

  if (length(x) > 0) {
    if (type == "warning") {
      warning(warn, "\n", paste(x[1:nlen], collapse = sep), endv, 
        call. = FALSE)
    } else if (type == "message") {
      message(warn, "\n", paste(x[1:nlen], collapse = sep), endv)
    } else if (type == "error") {
      stop(warn, "\n", paste(x[1:nlen], collapse = sep), endv, call. = FALSE)
    }
  }
}

#' Find closest match in a vector
#'
#' @param x numeric vector 
#' @param y vector of numeric values, each of which be matched with the closest
#'     value in \code{x}
#'
#' @return numeric vector of values of \code{x} that are closest to each 
#'     element of \code{y}
#' 
#' @keywords internal
#' @noRd
#' 
closestMatch <- function(x, y) {
    unlist(lapply(y, function(i) {
    x[which(abs(x-i) == min(abs(x-i)))]
  }))
}

#' Check relationship of two variables and find outliers with linear model 
#'
#' @param x dataframe 
#' @param x_var column name of explanatory variable
#' @param y_var column name of response variable
#' @param sigma number of standard deviations in residuals to act as threshold
#'     to define outliers
#'
#' @return Vector of row indices showing flagged relationship values
#' 
#' @export
#' 
relCheck <- function(x, x_var, y_var, sigma = 4) {
  
  # If no complete cases, break
  if (all(!complete.cases(x[[x_var]], x[[y_var]]))) {
    message("Unable to check relationship, no complete cases")
    return()
  }

  # Linear model
  mod <- lm(x[[y_var]] ~ x[[x_var]], na.action = na.exclude)

  # Extract residuals
  mod_resid <- resid(mod)

  # Extract standard deviation of residuals (sigma)
  mod_sigma <- sigma(mod)

  # Row index with residuals larger than acceptable sigma
  index <- unname(which(
    abs(mod_resid - mean(mod_resid, na.rm = TRUE)) >= (mod_sigma * sigma)
  ))

  return(index)
}

#' Generate random strings of letters and optionally numbers
#'
#' @param len length of string
#' @param num logical, should numbers be allowed in the string?
#'
#' @return character string
#' 
#' @examples
#' randString(5)
#' randString(8, num = FALSE)
#' 
#' @keywords internal
#' @noRd
#' 
randString <- function(len, num = TRUE) {
  samp <- c(LETTERS, letters)
  if (num) {
    samp <- c(samp, seq(0,9,1))
  }

  paste0(sample(samp, len, replace = TRUE), collapse = "")
}

#' Paste values together, replace NAs with blank, optional separator
#'
#' @param ... vectors or dataframe to be pasted together
#' @param sep separator between adjacent values in vectors
#' @param collapse separator between sets of values across vectors
#' @param unique logical, if TRUE duplicated values are removed
#' @param sort logical, if TRUE values are sorted
#'
#' @return character vector 
#' 
#' @examples
#' pasteVals(c(1,2,3,4,5), c(5,4,3,2,1), sep = "|")
#' pasteVals(c(1,2,3,4,5), c(5,4,3,2,1), c("a", "b", "c", "d", "e"), 
#'   sep = "|", collapse = "-")
#' pasteVals(data.frame(c(1,2,3,4,5), c(5,4,3,2,1)))
#' pasteVals(c(1,2,3,NA_real_,NA_real_), c(5,4,NA_real_,2,NA_real_))
#' pasteVals(c(6,2,3,4,5,6), c(6,5,4,4,2,1), unique = TRUE)
#' pasteVals(c(6,2,3,4,5,6), c(6,5,4,4,NA_real_,1), sort = TRUE, remna = FALSE)
#' pasteVals(c(6,2,3,4,5,6), c(6,5,4,4,NA_real_,1), sort = TRUE)
#' 
#' @export
#' 
pasteVals <- function(..., sep = "", collapse = NULL, 
  remna = TRUE, unique = FALSE, sort = FALSE) {
  ret <-
    apply(
      X = cbind(...),
      MARGIN = 1,
      FUN = function(x) {
        if (all(is.na(x))) {
          NA_character_
        } else {
          if (remna) {
            x <- x[!is.na(x)]
          }
          if (unique) {
            x <- x[!duplicated(x)]
          }
          if (sort) {
            x <- sort(x, na.last = TRUE)
          }
          paste(x, collapse = sep)
        }
      }
    )
  if (!is.null(collapse)) {
    paste(ret, collapse = collapse)
  } else {
    ret
  }
}

#' Remove duplicated flag values within strings
#'
#' @param x vector of character strings
#'
#' @return vector of character strings with duplicated values removed 
#' 
#' @examples
#' remDupFlag(c("FU", "FUFF", "FJJ"))
#' 
#' 
#' @export
#' 
remDupFlag <- function(x) {
  unlist(lapply(x, function(y) { 
    y <- unique(unlist(strsplit(y, split = "", fixed = FALSE, perl = TRUE)))
    if (all(!is.na(y))) { 
      paste(y, collapse = "")
    } else {
      y
    }
  }))
}

#' Fast rbind
#'
#' @param x list of dataframe objects
#'
#' @return dataframe
#' 
#' @details only accepts list of dataframes, no NULL values
#' 
#' @export
#' 
fastRbind <- function(x) { 
  x <- x[!sapply(x, is.null)]
  list2DF(lapply(setNames(
        seq_along(x[[1]]), names(x[[1]])), \(i)
      unlist(lapply(x, `[[`, i), FALSE, FALSE)))
}

#' Create arbitrary ID value combinations
#'
#' @param ... vectors of ID values, which are pasted together 
#' @param sep optional separator used by \code{paste()}
#'
#' @return vector of combined ID values 
#' 
#' @export
#' 
idValGen <- function(..., sep = NULL) { 
  if (is.null(sep)) { sep <- ":" }

  pasteVals(..., sep = sep, remna = FALSE)
}

#' Convert a decimal year into a date
#'
#' @param x numeric vector of decimal date values, precision to three decimal 
#'     places
#'
#' @return character vector of YYYY-MM-DD date values
#'
#' @details this function was taken from the \code{{lubridate}} package
#' 
#' @keywords internal
#' @noRd
#' 
dateDecimal <- function(x, tz = "UTC") {
  x <- as.numeric(x)
  Y <- trunc(x)
  s <- as.POSIXct(paste(Y, "01", "01", sep = "-"), tz = tz)
  e <- as.POSIXct(paste(Y + 1, "01", "01", "01", sep = "-"), tz = tz)
  seconds <- as.numeric(difftime(e, s, units = "secs"))
  frac <- x - Y
  e <- s + seconds * frac
  return(e)
}

#' Convert a census date into another format
#'
#' @param x vector of census dates
#' @param fmt string or vector of length \code{x} indicating format to 
#'     convert to, either "YYYY.YYY" (decimal year), "YYYY-MM-DD", "YYYY-MM", 
#'     or "YYYY"
#'
#' @return vector of reformatted census dates
#'
#' @details If reformatting to a date with greater resolution, e.g. "YYYY" to 
#'     "YYYY-MM-DD", the new date will assume the first day of the first month
#' 
#' @examples
#' x <- c("2022-11-11", "1900-05", "1950", "1984.453")
#' dateFormat(x, "YYYY")
#' dateFormat(x, "YYYY-MM")
#' dateFormat(x, "YYYY-MM-DD")
#' dateFormat(x, "YYYY.YYY")
#' dateFormat(x, c("YYYY.YYY", "YYYY", "YYYY-MM", "YYYY-MM-DD"))
#' @export
#' 
dateFormat <- function(x, fmt) {
  # Is fmt formatted correctly?
  if (length(fmt) != 1 & length(fmt) != length(x)) {
    stop("fmt must be a string or vector of length x")
  }

  # Are formats valid?
  if (any(!fmt %in% c("YYYY", "YYYY-MM", "YYYY-MM-DD", "YYYY.YYY"))) {
    stop("fmt must be one of 'YYYY', 'YYYY-MM', 'YYYY-MM-DD', 'YYYY.YYY'")
  }

  # Repeat format if only one value given
  if (length(fmt) == 1) {
    fmt <- rep(fmt, length(x))
  }

  # For each value in x
  out <- unlist(lapply(seq_along(x), function(y) {
    # If YMD, split into parts
    if (grepl("-", x[[y]])) { 
      y_split <- strsplit(as.character(x[[y]]), "-")[[1]]
      dec <- FALSE
      ylen <- length(y_split)
    } else {
      y_split <- x[[y]]
      dec <- grepl("\\.", y_split)
      ylen <- 1
    }

    # Pad values if necessary
    if (dec) {
      y_split <- as.numeric(y_split)
    } else if (ylen == 1 & !dec) { 
      y_split <- c(y_split, "01", "01") 
    } else if (ylen == 2 & !dec) {
      y_split <- c(y_split, "01")
    }

    # If a decimal, convert to a date
    if (dec) {
      y_split <- dateDecimal(y_split)
      y_split <- strsplit(format(y_split, "%Y-%m-%d"), "-")[[1]]
    }

    # Convert to chosen format
    if (fmt[y] == "YYYY") { 
      val <- as.character(floor(as.numeric(y_split[1])))
    } else if (fmt[y] == "YYYY-MM") {
      val <- paste(y_split[1:2], collapse = "-")
    } else if (fmt[y] == "YYYY-MM-DD") {
      val <- paste(y_split[1:3], collapse = "-")
    } else if (fmt[y] == "YYYY.YYY") {
      Y <- as.numeric(y_split[1])
      Yb <- as.POSIXct(paste0(Y, "-01-01"))
      Ye <- as.POSIXct(paste0(Y+1, "-01-01"))
      Yl <- as.numeric(Ye - Yb)
      d <- as.POSIXct(paste(y_split, collapse = "-"))
      val <- round(Y + (Yl - as.numeric(Ye - d)) / Yl, digits = 3)
    }
      
    # Return
    val
  }))

  # Return
  return(out)
}

#' Relative length encoding, treating NA as a real value
#'
#' @param x vector
#'
#' @return relative length encoding of vector
#' 
#' @examples
#' x <- c(0,0,0,NA,NA,NA,1,1,1)
#' rle(x)
#' rleNA(x)
#' 
#' @export
#' 
rleNA <- function (x)  {
  stopifnot("'x' must be a vector of an atomic type" = is.atomic(x))

  n <- length(x)
  if (n == 0L) {
    return(structure(list(
      lengths = integer(), values = x)
    ), class = 'rle')
  }
  y <- (x[-1L] != x[-n])
  y[is.na(y)] <- FALSE
  y <- y | xor(is.na(x[-1L]), is.na(x[-n]))
  i <- c(which(y), n)

  structure(list(
    lengths = diff(c(0L, i)),
    values  = x[i]
  ), class = 'rle')
}

