# CHANGELOG

Notable changes to the SEOSAW dataset, seosawr R package, SEOSAW manuals, [SEOSAW website](https://seosaw.github.io).

## TODO

* add `xy_method` == "tape" to CBN census (check this is actually the case)
* correct SSM 2018 data using Sally's script
* See `./doc/plan/seosaw_v4.docx` for more discussion of database updates
* Some values in `species_orig_binom` should really be in `species_orig_local`. Check for each dataset by trawling the species name change lookup table in the R cleaning script. John has created an R script which finds most of these bad names.
* Add stem data from two remaining forest plots to AUR dataset - waiting on Mathew Rees
* Add KLA_* stem diameter thresholds - waiting on PI
* Audit of MCL dataset, focussing on multi-census stem timelines
* DKS_1 subdivisions not working properly. 
* Rename `stem_status` and `stem_mode` to `status` and `mode`, respectively, in the stems table. `stem_` prefix is unnecessary
* Rename `damage_cause` and `damage_cause_notes` to `damage` and `damage_notes`, respectively, in the stems table. `_cause` suffix is unnecessary
* Rename `notes_stem` to `stem_notes` to match style of other notes columns

## [3.3] - UNRELEASED

* SHD_* replaced Dave Druve with Siphesihle Mbongwa as prinv
* MGR_* 2017 added Thom Brade as other_author
* NOC_* 2024 small stem measurements
* ABG_* 2024 stem and small stem measurements
* ALE_* changed stumps = FALSE (only counted, not sampled)
* Document clustering methods in `clusters.R` in compilation manual
	* Excludes genus indets
	* Excludes genera recorded in lt5 plots
	* Clusters plots with:
		* No non-naturalised exotics
		* gt80% stems identified to species
		* gt14 stems per plot
* Fixed bug in `recruitCheck()` which mistakenly identified stems as false recruits.
* Fixed bug in `latLongCoordGen()` which incorrectly ordered estimated lat-long stem coordinates if row order of plots was mixed within a dataset

## [3.2] - 2024-10-22

* KKM_* dataset added - Kenya Kibwezi Mbuvi
* ALE_* dataset added - Angola Lisima Escobar
* NZD_* dataset added - Namibia Zambezi Diesse
* ZKW_* dataset added - Zambia WeForest Katanino
* SST_* Satara plots modified to 4 x 1 ha plots each PI: Sally Archibald
* SSM_1 modified to 4 x SSM_1* plots PI: Sally Archibald
* MGR_* land use data updated by Penny Mograbi
* NKC_* dataset added - Kanovlei / Hamoye, Namibia. PI: Vera De Cauwer
* ADG_* dataset added - Bicuar degraded farmland plots. PI: John L. Godlee
* NCC_* error fixed in plot size, labelled diameters were actually radii
* Add data on canopy cover to ABG_*
* Add plot network map R script that everybody uses to `doc/network_map/`
* Add `pom_default` column to plots table indicating the default POM height for the plot.
* `growthIncrementGen()` renamed to `growthIncGen()`

## [3.1] - 2023-03-11

### Changes

* `speciesFill()` function added. Back-fills species names with most recent non-indet species name
* `pomImputGen()` function added. Interpolates and extrapolates missing POM values
* `statusImputGen()` function added. Interpolates and extrapolates missing stem status values
* `livestock_30_years` column added to plots table, logical. Similar to `farmed_30_years` (crops). Added to align with disturbance form.
* `geom_poly()` function, for checking validity of plot polygons, now allows objects of class `sfc_MULTIPOLYGON`, to account for plots consisting of multiple nearby polygons separated by non-plot area, e.g. DKS_2. 
* Removed `qual.R` from database compilation process. Most functions now produced as data quality flags in the database itself 
* Removed `spatial.R` from database compilation process. It's better if folk extract spatial data layers for themselves so they understand the subtleties of the data sources.
* `equivDiamGen()` function added to generate equivalent diameters of trees with multiple stems.
* `stemFillGen()` function added to add missing intermediate rows in a stem timeline based on plot census dates.

### Data

* DKS_* dataset overhauled with help from Mathew Rees. 2018 and 2021 censuses added.
* MNR_* 2019 census added.
* TIM_* dataset minor revisions
* TIN_* dataset added. A major overhaul of the TIM_* plots. Added as new dataset as revised plots are different dimensions and sampling strategy. 
* ABG_* added data from disturbance questionnaires 
* ZFC_* dataset added 
* NOC_* dataset added 

## [3.0] - 2022-10-29

### Changes

* `crownArea()` and `basalArea()` renamed to `crownAreaGen()` and `basalAreaGen()` to match other column generator functions
* Added disturbance ODK form and paper sheets. Translations into French and Portuguese. Made for SECO and asks many of the same questions that were previously included on the plot data sheet plus new categories.  
* column in plots table `herbivory_notes` renamed to `herbivore_notes` to match disturbance ODK form
* column in plots table `elephant_exclosure` removed, is covered in `exclosure`.
* columns in plots table added to match disturbance questionnaire: `fire_last_date`, `fire_fuel`, `fire_cause`, `cyclone`, `cyclone_last_date`, `drought`, `drought_last_date`, `wind`, `wind_last_date`, `earthquake`, `earthquake_last_date`, `landslide`, `landslide_last_date`, `flood`, `flood_last_date`, `sheep_grazing`, `clear_cut`, `mining`.
* column in plots table `fire_treatment_notes` renamed to `fire_notes`, now has any notes on fire regime or treatment, to match disturbance questionnaire.
* Dataset cleaning files, `data_raw/` and `data_clean/` directories now referred to by `dataset_id`, e.g. `ABG/*` rather than `godlee_bicuar/`.
* `tag_tree_stem` column added in plots table. Denotes whether tags applied to each tree, or to each stem on a tree. Column `tree_stem` renamed to `meas_tree_stem`, denotes whether measurements were taken for each stem, or just for example the largest stem on each tree.
* `next_census_check` column added in stems table. Contains text notes on what to check during next census.
* `largePlotSplit()` code improved to align with new census-centric plots table. Returns `subdiv_census_id` for plots and stems tables, to make `censusStemColGen()` for subdivided plots easier.
* `goodman13()` palm only allometry added. Stolen from BiomasaFP.
* `tiepolo02()` tree-fern only allometry added. Stolen from BiomasaFP.
* `fastRbind()` function added. Speeds up appending tables by about 5x. Applied within functions where `do.call(rbind, ...` with a list of dataframes normally used. Can't use for `{sf}` dataframes or when list contains `NULL` objects.
* `points.shp` removed from database and `exportData()`. Can be easily compiled from plots table `longitude_of_centre`, `latitude_of_centre`, `plot_id`.
* `cleaning/functions.R` removed. `old_plot_col_repl()` moved into `{seosawr}` and renamed to `plotColNamesV1()`, now only renames columns, doesn't adjust plot radius values. Dataset cleaning scripts updated to reflect these changes.
* `heightDiamFit()` function to fit diameter height models. `heightEst()` to estimate height using a fitted model object.
* `chaveE()` to retrieve Chave's environmental index (E), a thin wrapper around `BIOMASS::computeE()`. `chaveEMod()` can be used to estimate height using diameter and 'E'.
* `feldRegion()` to retrieve Feldpausch's region index, a thin wrapper around `BIOMASS::computeFeldRegion()`.
* `growthIncrementGen()` simple function to calculate inter-census diameter growth for stems.
* `woodDensity()` now returns mean wood density `wood_density_mean`, standard deviation of wood density `wood_density_sd`, the level at which wood density was estimated `wood_density_lev`, and the number of individuals used to estimate the wood density `wood_density_n`.
* `saplings_sampled` -> `small_stems_sampled` to match terminology elsewhere.
* `understory_sampled` -> `ground_layer_sampled` to match terminology elsewhere.
* `stem_mode` flag changes:
	* `bracket_fungi` column removed -> "k"
	* `on_termites` column removed -> "m"
	* `canopy_climbers` and `stem_stranglers` columns removed -> "c"
	* `liana` column removed -> "w"
	* `standing_fallen` column removed -> "s" + "f"
	* "g" - hollow
	* "z" - sick looking stems.
	* "y" - wounded stems.
	* "j" - stems with peeling bark.
	* "t" - stumps, i.e. broken below the POM.
* `species_name_sanit` column added in stems table. Species names after automated cleaning provided by `sanitizeTaxa()`, but before `{taxize}` checks.
* `stem_timeline_cont` column added in plots table, to indicate where stems can be matched between the particular census and previous censuses.
* `photos` column added in plots table, ";" separated list of photo filenames which are stored in `data_raw/`
* `abandoned` column added in plots table, to indicate the final census of a permanent plot before abandonment.
* `damage_cause` flag changes:
	* "i" - frost. Of particular use in Holdo Hwange data.
	* "a" - animals, not elephants.
	* "o" - other, to be described in `damage_cause_notes`.
	* "l" - lightning, replaces column `lightning`.
* `damage_cause_human` -> `damage_cause_notes`. Now a `note_val` column, allows more description and doesn't limit to human damage.
* `max_diam_thresh` and `max_height_thresh` removed. They aren't being used and are a pain to code.
* `first_census`, `last_census`, `all_dates` removed. `census_date` is adequate now that plots table is really the census table. 
* `speciesNew()` removed, incorporated into `speciesCheck()`
* `seosawFamilyGenExport()` and `seosawSpeciesGenExport()` combined to `seosawTaxaExport()`. Output format improved.
* `decDMS()` and `dmsDeg()` added to convert decimal degrees to degrees minutes seconds, and vice versa.
* `speciesGen()` renamed to `speciesCheck()`
* `abMatGen()` ~3x speed increase using `{data.table}`
* `cjb_apd` is a new data object holding species level synonyms taken from the CJB African Plant Database. `cjbQuery()` has been removed, functionality now covered by `synonymyFix()`. This is because CJB have changed their website, and have removed their API.
* `polyGen()` renamed to `polyEstGen()`. New `polyGen()` function to generate plot polygons from known corner locations, using `sf::st_convex_hull()`.
* `deathCheck()` renamed to `stemLogicCheck()` because it deals with more logical inconsistencies than just those found in mortality status.
* `wound_area` column added to stems table, to incorporate wound data.
* `stem_mode` and `damage_cause` can now accept multiple flags for a single stem.
* `pasteVals()` can be used to intelligently concatenate strings, mostly used to create stem mode and damage cause flags.
* Column generator functions no longer assert class attributes as this inteferes with `dplyr::*_join()` functions. [See](https://github.com/tidyverse/dplyr/issues/5629).
* Removed requirement for `tree_id` and `stem_id` to be an integer. Tree ID should be the tag ID of a stem on the tree. Tree and stem IDs are now persistent and never change, while `tag_id` is allowed to change among censuses, to deal with replacement tags. Removed function `idClean()` as stem ID and tree IDs are now created differently. `measurementIDGen()` replaces some of the functionality. 
* Many data sent to SEOSAW contain the exact date the stem was measured, so `census_date` has been updated to allow dates at the resolution of the year (e.g. 2022), the year and month (e.g. 2022-06), or the year, month and day (e.g. 2022-06-06). Where necessary in functions `census_date` is converted into a year using `dateFormat()`.
* Removed functions `combineSpecies()` and `speciesFormat()`, replaced with `sanitizeTaxa()` which has more functionality.
* Added `cwdcolClass()`, `smallStemColClass()`, `groundLayerColClass()` functions to check validity of columns in CWD, small stems, and ground layer data tables, respectively. Added `cwd_columns.csv`, `small_stems_columns.csv`, `ground_layer_columns.csv` which detail the contents of each column in these data data tables.
* Removed function `agbGen()` - individual biomass functions are good enough. Canonical database uses `chave14()` as standard.
* `chave14()` fixed so it doesn't return 0 AGB for stems outside plot minimum stem size thresholds.
* Removed `plot_data_template.csv` and	`stem_data_template.csv`. Uninformative. Same information held in `stem_columns.csv` and `plot_columns.csv` in a more digestable format.
* `measurement_date` -> `census_date`, to match column name in plots table.
* Various checking functions added `stemTableCheck()`:
	* `tagUniqueCheck()` - Is the tag unique within a census?
	* `resurrectCheck()` - Has the stem resurrected (`stem_status` "d" -> "a")?
	* `diamChangeCheck()` - Has the diameter changed drastically since the last census?
	* `speciesChangeCheck()` - Has the species changed since the last census?
	* `stemNCheck()` - Does the tree have too many stems?
	* `recruitCheck()` - are measurements marked as recruits truly the first time the stem has been observed? Are measurements not marked as recruits definitely not the first census they have been observed (excluding the initial census on the plot)?
* `exampleDataGen()` rewritten, better handling of multi-census data. Can now also export example CWD, small stems, ground layer data. `exampleDataExport()` rewritten to account for this.
* Updated `exportData()` to deal with CWD, small stems, and ground layer data. Also with the flags returned by the `*TableCheck()` functions.
* Rasterised graph images in dataset manual so the pdf is smaller.
* Updated ODK forms to reflect changes in this version.
* Minor updates to stems field sheet and all translated versions to include height, decay. Removed bracket fungi (is a stem mode flag). Updated footer flag cheat boxes to include new flags.
* Added `stumpsSampledCheck()` to check consistency of `stumps_sampled` in plot data and stump specific columns in stems data.
* Added `stump_diam_base` and `stump_diam_top` to record data on stumps in stems table.
* Updated the field manual. Field manual now has compromised plot survey. This version has been translated.
* Soil manual: New soil protocol. No older verions uploaded as changes were made on a live word document.
* Field cheat sheet: Two page resource sheet that summarizes key points and codes when collecting stem data. 
* Plot data sheet: New format aligns more closely with forestplots.net and does not include disturbance questions, only should be used for plot set-up. Word and PDF uploaded.

### Data 

* AUR_* 2 new 50x50 m plots in northern Angola from Mathew Rees. Two more from forest to come later.
* ABG_* plots updated with 2021 census. Three new plots ABG_17, ABG_18, ABG_19. Added small stems and ground layer data.
* MGR_* 2021 census
* TKW_* 2021 census. Went back through TKW v1 data and converted notes to stem_mode and damage_cause flags, other fixes too. 
* SSM_1 cleaned further. No new data added. 
* SST_* data changed significantly. No longer small circle plots (these were subplots), instead larger 5 ha square plots with more SEOSAW-like data. Essentially a different dataset, different PI.
* Added KKT_* Tiva KEFRI site. 3 PSPs, with small stems and stumps.
* Added UEK_* ICRAF farm trees dataset.
* KLS_* extracted canopy dimensions and height out of stems notes. Adjusted tree ID and stem ID values.
* "Strychnos_indet" changed to "Strychnos indet" in MCF plots.
* `dataset_descrip.yaml` contains long-form prose descriptions of each dataset. Could benefit from more detail.
* ACM_41 removed from stems data. No plot data available.
* Split Zimbabwe ZFC data into separate datasets: ZPF, ZRF, ZGF, ZNF, ZHF. Included 2021 data for ZPF.

## [2.12] - 2021-03-17

### Added

* Made a `fpc == 1` version of the community matrix, in `compile_all.R`. File is in `species_plot_matrix_nonest.csv`
* ODK template form for recensusing a plot - `stems_recensus.xls`
* McNicol Kilwa dataset. 56 one-off circular 25 m radius plots. - `TKN`
* Regional report RMarkdown template. Feed it a .shp polygon to generate a report of SEOSAW's involvement in the area.
* Dataset report Rmarkdown template. Feed it a dataset ID (e.g. "SSM") to generate useful data visualisations for that dataset.
* Added `canopy_climbers`, `stem_stranglers`, `on_termites`, `liana` questions to all stems ODK forms
* Added `alive` question to all stems ODK forms. This question becomes relevant if topkilled and not-resprouting
* `polyPlotCheck()` to see if polygon areas match roughly with `plot_area` generated from `plot_length`, `plot_width`, `plot_radius`. Also that distance between centres of plot in plots data is similar to centre of polygon.
* `baobab` column in plots data, to check if a plot contains _Adansonia digitata_, which can mess up biomass estimations.
* New "data quality" table output in `data_out/plot_qual.rds`, with calculated values to show data coverage and number of measurements with possible errors in the dataset. Table generated by `cleaning/qual.R`.
* `saplings_sampled` - TRUE/FALSE whether stems smaller than the minimum diameter threshold were counted.
* `wood_density` - wood density per stem taken from Zanne et al. 2009 global wood density database. Calculated with `stemExtraColGen()`, which sources `BIOMASS::getWoodDensity()`

### Changed

* `plots.csv` is now organised as one row per census per plot, with a new column `census_id` which acts as the primary key. `plots_latest.csv` is a subsidiary of `plots.csv` which only contains plot-level statistics for the most recent census. This change is seen as a baby step towards the eventual goal of having a separate "census" table (census level calculated statistics) and "plot" table (plot level metadata). Additionally, this way the default object `plots.csv` contains data on all censuses, so users can flexibly subset by census.
* Circle polys generated with `polyGen()` were previously too large due to an error in the way `sf::st_buffer()` was applied.
* Nested subplot measurements removed from `CBN_*` as they were surveyed inconsistently across censuses and caused aritificial inflation of stocking density
* Removed four plots in `MCF` with unknown plot dimensions.
* `plot_diameter` -> `plot_radius` - plays better with arbitrary `{sf}` functions and more natural to measure in the field in a circular plot.
* Removed all `SHS` plots from compiled dataset.
* Removed `ZPF_10`, `ZPF_11`, `ZPF_12`, `ZPF_13`, `ZPF_14`, `ZPF_15`, `ZGF_30`, `ZGF_33`, `ZGF_35`, `ZGF_37`, `ZGF_45` due to lack of plot location
* Checked through all multi-census datasets and removed backdated recruits, with NA diameter measurements
* Minor updates to stems ODK form variants to flag diameter values >100 cm
* Stems recensus ODK form now handles tags not in original data more gracefully.
* Typo in species name correction in `MGR`: corrected from "Searsia chiridensis" to "Searsia chirindensis"
* Typo in species name correction in `MNF`: corrected from "Rytiginia monantha" to "Rytigynia monantha"
* Fixed some species typos in `MNI`: "Scorodophleus fischeri" -> "Scorodophloeus fischeri", "Trilephium madagascariensis" -> "Trilepisium madagascariense", "Xanthocersis zambesiaca" -> "Xanthocercis zambesiaca", "Zanha golongensis" -> "Zanha golungensis",
* `name_gnr` -> `species_name_gnr` and `name_cjb` -> `species_name_cjb`. More consistent with other taxonomy columns and more informative.
* Updated documentation on taxonomy in dataset manual
* `data(acacia)` column names changed to `c("original", "corrected")`, to be consistent with other lookup tables
* `MCF_2108` contains one individual of "Strychnos_indet", corrected to "Strychnos indet"
* `height_method` for all `ABG` plots updated to "rangefinder"
* `MMB_9` and `MMB_10` `tree_id` values were not assigned correctly, due to duplicated "tag" values. These have now been fixed by stratifying by `subplot_id` when creating `tree_id` values
* Fixed tree ID issues for `ABG` by looking at original paper sheets. Some `tree_id`'s comprised multiple species.
* `CBN` `height_method` == "direct" (stick)

### Unsolved ssues

* `MCL`: possible that some stems should be dead in future censuses, as they are without diameter measurements. It's also possible that the stem IDs are completely wrong in the 2000 census. This issue is also present in previous versions
* `SBM`: stem diameters are actually circumferences, uncorrected as of 2021-03-16

## [2.11] - 2021-02-17

### Added

* Code to PI names from SEOSAW plots dataset for use in website
* `dateCheck()` to see if `first_census`, `last_census` and `all_dates` are logically consistent
* New columns 
	* >=20 cm diameter variants for diameter, height, stocking density, note names changed from `gt` to `ge` to reflect "greater than or equal to" rather than the old "greater than":
		* `diam_ge20_max`
		* `diam_ge20_min`
		* `diam_ge20_mean`
		* `diam_ge20_median`
		* `diam_ge20_sd`
		* `height_ge20_max`
		* `height_ge20_min`
		* `height_ge20_mean`
		* `height_ge20_median`
		* `height_ge20_sd`
		* `stocking_ge20_ha`
	* >=3,5,10,20 cm diameter variants of `agb_ha`:
		* `agb_ge3_ha`
		* `agb_ge5_ha`
		* `agb_ge10_ha`
		* `agb_ge20_ha`
	* `height_per95` - the 95th percentile of stem height
	* `diam_scale`, `diam_scale_se`, `weibull_scale`, `weibull_shape`, `weibull_scale_se`, `weibull_shape_se` - columns which describe the shape of stem diameter class distributions for each plot. See `diamWeibullGen()` and `diamScaleGen()` functions which define these columns.
	* `coarse_woody_debris_sampled` - TRUE/FALSE whether plot had coarse woody debris sampled.
* `plots_subdiv.csv` and `polys_subdiv.shp` provide 1 ha subdivisions of plots >2 ha, rectangular and divisible by 100x100m squares. `largePlotSplit()` does the splitting. `plots_subdiv.csv` provides latest census calculated fields (e.g. `agb_ha`) for these new subplots. A census level version of `plots_subdiv.csv` may be added later.
* `gridCoordGen()` - approximates an XY grid coordinate system in ractangular plots which have GPS stem positions
* `plot_clusters.csv` - Assigns plots to clusters based on genus stem abundances, using Ward clustering. `clusters_dom.csv` gives the dominant genera for each cluster., based on mean stem abundance across all plots in cluster.
* `build.sh` - shell script for compiling a version of the SEOSAW dataset
* Dataset-level long-form descriptions of each dataset in the Dataset Manual
* `doc/versions.csv` spreadsheet which takes snapshots of the versions of all the versioned objects in SEOSAW to keep them together

### Changed

* Columns which contain a list of items now separate items by ";":
	* `prinv`
	* `other_authors`
	* `all_dates`
* `other_authors` and `prinv` values cleaned to maintain consistency. E.g. "Godlee, JL" -> "Godlee J. L.", "John Godlee" -> "Godlee J. L." 
* Backdated recruits with `diam` == NA in `MGR_*` removed from dataset
* Forward dated unmeasured stems removed from various plots: `CBN_3`, `CBN_6`, `MNR_14`, `MNR_41`, etc.
* Corrected `all_dates` for a number of plots which were causing issues with the new `subsetLatest()` code: `MAR`, `MCL`, `ZRF`, `CBN`, `TIM`
* `first_census` and `last_census` auto-generated from `all_dates` by `plotExtraColGen()`
* `ba` and `agb` (not per ha) cols removed from plots table as they can lead to confusion.
* Functionality from `emptyCatch()` added into `fillNA()`, handles conversion of Inf, NaN, NA, "N/A", "na", "   ", and optionally "0", to NA. Maintains class of original vector.
* `CLP_*` plots changed to `DLP_*` to reflect location in the Democratic Republic of Congo (D), rather than the Republic of Congo (C).
* Fixed `fpc` values for `SHD_*` plots. Previously AGB estimates were falsely inflated. New AGB values are much lower than previous.
* Overhauled functions to generate fake data, now in one function called `exampleDataGen()`.
* Overhauled `plotStemColGen()` (was `plotStemColumnGen()`) so that it handles empty entries more consistently, correctly differentiates which columns should be 0 if empty (e.g. `n_stems`) and which should be NA if empty (e.g. `lorey_height` (e.g. `lorey_height`)).
* Changes to field paper data sheets `blank_plot.pdf` and `blank_stem.pdf`. These forms are now given version numbers and archived in the same way as manuals on the website with a `_latest` version.
	* Removed max thresholds for diameter and height. 
	* Added explicit prompts for nested plot dimensions and sampling thresholds
	* Added site name 
	* Location data now for each corner
	* Added "Coarse woody debris" to sampling TRUE/FALSE section
	* Added "PI" 
	* Added "A - alive" to stem status table
	* Added "Trees tagged" 
	* Added form version number and field manual version to form footer on every page

### Changed

* `other_authors`, `all_dates` to sep by `;`
* `broken_per_remain` -> `broken_remain`

## [2.10] - 2021-01-21

### Added

* Extra check during `dataExport()` to make sure stems and plot tables have the full complement of columns
* ODK forms, one for plot data and one for stem data. Found in `doc/manuals/forms/odk/`. Alternative variants of stems form which is compatible with Kobotoolbox, doesn't use the auto-populating species list feature, instead takes a pre-defined list of species, and another which uses the CJB african plant species list.
* Added function to check for duplicated measurement IDs (`measurement_id`) during cleaning - `measurementIDCheck()`
* New datasets: 
	* Wilson Mugasha: 12 plots part of a woodland regeneration experiment near Morogoro, Tanzania. `TKM_*`
	* Antje Ahrends: Transects made into plots in Tanzanian coastal forest (?). `TLA_*`
* New columns:
	* TRUE/FALSE columns, to keep in line with ForestPlots.net:
    	* `recruit` 
    	* `on_termites` 
    	* `lightning` 
    	* `canopy_climbers` 
    	* `stem_stranglers` 
    	* `stump` 
    	* `bracket_fungi`
	* `manipulation_experiment_notes` - provide notes of experimental treatment applied to plot.
	* `cambium` - proportion of stem surface with cambium exposed between 0-2 m from ground
* New options in columns, to keep in line with ForestPlots.net:
	* `stem_mode`: "x" - Presumed dead, location not found e.g. due to poor maps
	* `damage_cause`: "l" - Lightning
* New spatial data columns: `distance_road` and `type_nearest_road` - distance to nearest road and road type of nearest road, from Meijer et al. Global patterns of current and future road infrastructure. (2018).�Environmental Research Letters.

### Changed

* Repository made open access
* `seosawr` package vignette, data contribution manual, and dataset description joined together into one "dataset manual" as a .pdf file only, using Sweave instead of RMarkdown
* Re-organised manuals and docs into `doc/`
* Field sheets and data entry templates now found in `doc/manuals/forms/`
* Column names:
	* `sampling_design_fp_code` -> `sampling_design_code` - no reason why this column should be limited to ForestPlots protocols
	* `stem_decay` -> `decay` - "stem" is redundant as in stems table
	* `plot_slope` -> `slope` - "plot" is redundant as in plots table
	* `plot_aspect` -> `aspect` - "plot" is redundant as in plots table
* Maiato Cusseque plots FPC set to 1. Previously was an error with FPC increasing by row number due to dplyr update
* Added "other" as a factor option to `damage_cause_human`, `height_method`, `diam_method`, `xy_method`, `slope_method`, `canopy_cover_method`
* `vecExtract()` renamed to `vectorExtract()` to match style of `rasterExtract()`
* In `speciesGen()`, if genus is a family and species is indet, record genus as "Indet". Based on list of known families generated by `seosawFamilyGen()`. 
* Fixed issue with duplicated measurement IDs in 'MAR_*', 'MCL_*', 'ZCC_*', 'DKS_*', 'TKX_*', 'TKW_*'
* All factor codes (`alive`, `stem_status`, `standing_fallen`, `stem_mode`, `damage_cause`) replaced with lower case variants. Upper case "F" values can cause `read.csv()` to interpret them as logical "FALSE", which is undesirable. Column constructor functions now automatically convert to these lower case variants where they match 
* `speciesGen()` now corrects "indet" and "Indet" to "Indet indet"
* `ACM_*` plots have corrected biomass, previously erroneously low due to many `stem_id == NA`
* `analysis` directory removed from repository
* Spatial data column `ma_city` replaced with `travel_time_city` and  `travel_time_settlement`, using the updated data from Nelson et al. (2019) A suite of global accessibility indicators. Scientific Data. City classed as >50000 people, settlement as >5000 people

## [2.9] - 2020-11-07

### Added

* SEOSAW region polygon as data object in `{seosawr}`: `data(seosaw_region)`
* SEOSAW region bounding box as data object in `{seosawr}`: `data(seosaw_bbox)`
* Fire return interval now applicable to natural and treatment fires.
* `CHANGELOG.md` to track changes in SEOSAW database, `{seosawr}` and the website.
* New columns
	* plots: `plot_aspect` - column with degree angle value
* Set of blank SEOSAW style datasheets for the field, with companion data entry sheets, and a program to create re-census field datasheets from existing SEOSAW style data. Tested with 'ABG_*', 'TKW_*', and 'CAR_*' datasets. Blank sheets hosted on website, program still private.
* Polygons now auto-generated for those plots which have no polygons, based on `longitude_of_centre`, `latitude_of_centre`, `plot_shape`, `plot_diameter`, `plot_length`, `plot_width`.
* `circlePolyGen()` -> `polyGen()` - now handles circle and rectangle plot shapes, provided they aren't missing any necessary data
* `versionCheck()` to facilitate checking of plot datasets

### Changed

* Column names:
	* plots: `fire_treatment_return_interval` -> `fire_return_interval` - allow non-treatment estimates of fire return interval
	* plots: `fire_control` -> `fire_treatment` - "control" implied that fires are only suppressed
	* plots: `method_canopy_cover` -> `canopy_cover_method` - for consistent ordering of terms
	* plots: `method_canopy_cover_notes` -> `canopy_cover_method_notes` - for consistent ordering of terms
	* stems: `xy_latlon_method` -> plots: `xy_method` 
    * plots: `mean_plot_slope` -> `plot_slope`
	* stems: `method_diam` -> `diam_method` - for consistent ordering of terms
	* stems: `method_height` -> `height_method` - for consistent ordering of terms
* Relaxed restrictions on `plot_id`, just three unique letters and an integer, doesn't have to be initials.
* Spatial data layers removed from `{seosawr}`, now stored separately and extracted using data in `seosaw_data/data_out` rather than `seosaw_data/data_clean/*` 
* Max and min lat-long coordinates of plot locations are now defined according to `data(seosaw_bbox)`.
* Abundance matrix generator function (`abMatGen()`) replaced. Removed dependency on `{dplyr}` and `{tidyr}`. Faster and more terse code.
* Dataset compilation script `compile.sh` replaced with more flexible `Makefile`, but this feature is still unfinished
* Website contact form migrated from SimpleForm to Formspree
* `vecExtract()` uses faster matching with `st_intersects()`, rather than `st_join()`
* `EPSG2CRS` now returns WKT by default, rather than proj4string.
* All spatial extraction functions can now handle non-EPSG CRS.
* `stemTreeIDClean()`, `tree_id()`, `stem_id()`, `measurement_id()` -> `idGen()`. Removes bloated dependencies on `{dplyr}`, better handling of NA `stem_id` and `tree_id` values
* Re-censuses are now cleaned in the same site-level cleaning script as original data, to make cross-checking easier.
* `NA` for `polygons` in "SSM" "MCF" "MNF" "ZHH" "MCL" "TIM" "DKS" "ZWP" "MNR" "MGR" "ZKS" "SBT" "ZGF" "ZPF" has been corrected, all now either TRUE or FALSE.
* Corrected nesting and `fpc` values for Zimbabwe FC and Twine SUCSES datasets
* `mstems` and `mplots` example datasets re-created to reflect above changes
* Better stem matching between censuses in Kilwa and Nhambita datasets, thanks to Thom Brade
* Estimates of basal area, AGB, number of stems reduced in `DKS_1` due to fixing census dates
* Lower AGB, basal area, in SHD plots due to fixed `fpc` 
* New plots in Mozambique NFI (`MNI`) dataset and Vera De Cauwer's Namibia dataset (`NCC`)
* Archibald Kruger dataset vastly reduced `n_determ_species` because most recent census only looked at large trees. Plot level statistics are generated from most recent census only


## [2.8] - 2020-09-10

### Added

* Vera De Cauwer's Angola/Namibia plot data
* Alternative stem coordinate system for circular plots - `distance` and `angle` from plot centre.
* `permanent` column, logical which defines whether a plot is permanent or not. 
	* Website dataset description reflects addition of `permanent` column
	* Website map reflects new `permanent` column.
* Moved some website-making scripts to the SEOSAW Bitbucket repo.

### Changed

* Column names:
	* `first_census_date` -> `first_census`
	* `last_census_date` -> `last_census`
* Kilwa plots: `TKW_4`, `TKW_19`, `TKW_22`, included in polygons dataset. Previously excluded because plot centre coordinates didn�t fall within the polygon co-ordinates, this requirement has now been relaxed and instead matches based on the nearest polygon. 
* Subtly changes the description of nested plots, particularly plot minimum diameter threshold (`min_diam_thresh`). 
* Data cleaning scripts now clean polygons per site alongside other data, rather than in a polygons-only script. 

