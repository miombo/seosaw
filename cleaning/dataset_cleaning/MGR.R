# Clean Ryan Nhambita data
# John Godlee (johngodlee@gmail.com)
# Last updated: 2022-11-12

# Packages
library(dplyr)
library(seosawr)
library(readxl)
library(sf)

# Import data
plots <- read.csv(file.path(raw_dir, "MGR/2022_nhambita_plots_table.csv"))

stems_old <- read.csv(file.path(raw_dir, "MGR/27_mozambique_nhambita_ryan_clean.csv"))
stems_2019 <- readRDS(file.path(raw_dir, "MGR/2019_brade_nham.rds"))
stems_2021_matched <- readRDS(file.path(raw_dir, "MGR/2021_recensused_stems_clean_matched_v2.rds"))
stems_2021_recruit <- readRDS(file.path(raw_dir, "MGR/2021_new_recruit_stems_clean_v2.rds"))
stems_2021_nomatch <- readRDS(file.path(raw_dir, "MGR/2021_unmatched_stems.rds"))
stems_2021_lost <- readRDS(file.path(raw_dir, "MGR/2021_lost_stems.rds"))

polys <- st_read(file.path(raw_dir, "MGR/psp_corners_Project.shp"))

# Plot cleaning
plots_clean <- plots %>% 
  rename(nested_1 = nested) %>% 
  mutate(
    tag_tree_stem = "stem",
    dataset_id = "MGR",
    nested_2 = FALSE, 
    nested_3 = FALSE,
    subdiv = FALSE,
    file_plots = "2022_nhambita_plots_table.csv",
    file_stems = "2019_brade_nham.rds;2021_recensused_stems_clean_matched.rds;2021_new_recruit_stems_clean.rds;2021_unmatched_stems.rds;2021_lost_stems.rds",
    photos = case_when(
      plot_id == "MGR_6" ~ paste(c(
        "MGR_06_grass-layer_Sep2021.JPG",
        "MGR_06_grassy_termite_mound_Sep2021.JPG",
        "MGR_06_grazing-lawn_Sep2021.JPG",
        "MGR_06_grazing-lawn2_Sep2021.JPG",
        "MGR_06_large-tree-grazing-lawn_Sep2021.JPG",
        "MGR_06_taken-from-ground-to-demonstrate-grass-height_unburnt.JPG",
        "MGR_06_unburnt-open-canopy_Sep2021.JPG",
        "MGR_06_unburnt-open-canopy2_Sep2021.JPG"), collapse = ";"),
      plot_id == "MGR_7" ~ "MGR_07_burnt_Sep2021.JPG",
      plot_id == "MGR_8" ~ paste(c(
        "MGR_08_burnt_Sep2021.JPG",
        "MGR_08_burnt2_Sep2021.JPG",
        "MGR_08_burnt3_Sep2021.JPG"), collapse = ";"),
      plot_id == "MGR_13" ~ paste(c(
        "MGR_13_burnt_large-trees_Sep2021.JPG",
        "MGR_13_burnt_riparian-edge2_Sep2021.JPG",
        "MGR_13_burnt_Sep2021.JPG",
        "MGR_13_burnt-river-bank_Sep2021.JPG",
        "MGR_13_burnt-shrub-layer_Sep2021.JPG",
        "MGR_13_burnt-uprooted-tree_Sep2021.JPG",
        "MGR_13_riparian-edge_Sep2021.JPG"), collapse = ";"),
      TRUE ~ NA_character_),
    pom_default = 1.3,
    other_authors = case_when(
      census_date == 2017 ~ "Thom Brade",
      TRUE ~ NA_character_)
    ) %>% 
  colGen(., plotColClass())

plots_check_list <- plotTableCheck(plots_clean)
plots_check <- plots_check_list[[1]]
plots_flags <- plots_check_list[[2]]

# Polygons
plots_sf <- st_as_sf(plots_check, 
  coords = c("longitude_of_centre", "latitude_of_centre")) %>%
  dplyr::select(plot_id) %>%
  st_set_crs(4326)

polys_wgs <- st_transform(polys, 4326)

polys_clean <- st_join(polys_wgs, plots_sf, st_nearest_feature) %>%
  dplyr::select(plot_id)

polyPlotCheck(plots_check, polys_clean)

# Stem cleaning
stems_2019_clean <- stems_2019 %>%
  dplyr::select(
    plot_id = plotcode,
    census_date = year, 
    tag_id = stem_id_u, 
    tree_id = base_id_u, 
    x_grid = x, 
    y_grid = y, 
    pom,
    diam,
    species_orig_binom = spp_cln,
    -genus,
    stem_status = alive,
    notes_stem = wound) %>%
  mutate(
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    tag_id = gsub(".*_", "", toupper(.$tag_id)),
    stem_id = tag_id, 
    tree_id = gsub(".*_", "", toupper(.$tree_id)),
    pom = pom / 1000,
    diam = diam / 10,
    diam_method = "dbh_tape",
    fpc = 1, 
    notes_stem = case_when(
      notes_stem == TRUE ~ "wounded", 
      TRUE ~ NA_character_),
    stem_mode = case_when(
      grepl("N", tag_id, ignore.case = TRUE) & !is.na(diam) ~ "n",
      TRUE ~ NA_character_)
    ) %>% 
  filter(  # Remove ghost censuses. Known census dates in plots table
    (plot_id == "MGR_1" & census_date %in% c(2006)) | 
    (plot_id == "MGR_10" & census_date %in% c(2006, 2007)) | 
    (plot_id == "MGR_11" & census_date %in% c(2006, 2007)) | 
    (plot_id == "MGR_12" & census_date %in% c(2006, 2007)) | 
    (plot_id == "MGR_13" & census_date %in% c(2006, 2007, 2008, 2010, 2017, 2021)) | 
    (plot_id == "MGR_14" & census_date %in% c(2006, 2007, 2010, 2017, 2021)) | 
    (plot_id == "MGR_15" & census_date %in% c(2006, 2007, 2008, 2010, 2017)) | 
    (plot_id == "MGR_2" & census_date %in% c(2006, 2007, 2009)) | 
    (plot_id == "MGR_3" & census_date %in% c(2006, 2007, 2009, 2010)) | 
    (plot_id == "MGR_4" & census_date %in% c(2006, 2007, 2009)) | 
    (plot_id == "MGR_5" & census_date %in% c(2006, 2007, 2008, 2010, 2017, 2021)) | 
    (plot_id == "MGR_6" & census_date %in% c(2006, 2007, 2008, 2010, 2017, 2021)) | 
    (plot_id == "MGR_7" & census_date %in% c(2006, 2007, 2010, 2017, 2021)) | 
    (plot_id == "MGR_8" & census_date %in% c(2006, 2007, 2008, 2010, 2017, 2021)) | 
    (plot_id == "MGR_9" & census_date %in% c(2006, 2007))) %>% 
  filter(  # Remove ghost stems from censuses before individual recruited
    !(plot_id == "MGR_5" & tag_id %in% c("20","207","209","11","248",
        "253","12","354","364", "396","400","44","135","505","53","54",
        "55","56","60","89","169") & census_date %in% c("2007", "2008")),
    !(plot_id == "MGR_4" & tag_id %in% c("466N","574N","610")),
    !(plot_id == "MGR_2" & tag_id %in% c("21","336","343")),
    !(plot_id == "MGR_14" & tag_id %in% c("281","283") & 
      census_date %in% c("2010","2017")),
    !(plot_id == "MGR_10" & tag_id == "908" & census_date == "2006"),
    !(plot_id == "MGR_10" & tag_id == "908" & census_date == "2007"),
    !(plot_id == "MGR_10" & tag_id == "926" & census_date == "2006"),
    !(plot_id == "MGR_10" & tag_id == "926" & census_date == "2007"),
    !(plot_id == "MGR_11" & tag_id == "100-110" & census_date == "2007"),
    !(plot_id == "MGR_11" & tag_id == "115-125" & census_date == "2007"),
    !(plot_id == "MGR_11" & tag_id == "240-250" & census_date == "2006"),
    !(plot_id == "MGR_11" & tag_id == "440-450" & census_date == "2006"),
    !(plot_id == "MGR_11" & tag_id == "447" & census_date == "2006"),
    !(plot_id == "MGR_11" & tag_id == "447" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "121-150" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "121-150" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "151-180" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "151-180" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "171" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "171" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "171" & census_date == "2009"),
    !(plot_id == "MGR_2" & tag_id == "200N" & census_date == "2009"),
    !(plot_id == "MGR_2" & tag_id == "230" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "230" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "230" & census_date == "2009"),
    !(plot_id == "MGR_2" & tag_id == "30-60" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "30-60" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "30-60" & census_date == "2009"),
    !(plot_id == "MGR_2" & tag_id == "58N" & census_date == "2009"),
    !(plot_id == "MGR_2" & tag_id == "85" & census_date == "2006"),
    !(plot_id == "MGR_2" & tag_id == "85" & census_date == "2007"),
    !(plot_id == "MGR_2" & tag_id == "85" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "300-350" & census_date == "2006"),
    !(plot_id == "MGR_3" & tag_id == "300-350" & census_date == "2007"),
    !(plot_id == "MGR_3" & tag_id == "300-350" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "445" & census_date == "2006"),
    !(plot_id == "MGR_3" & tag_id == "445" & census_date == "2007"),
    !(plot_id == "MGR_3" & tag_id == "445" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "472" & census_date == "2006"),
    !(plot_id == "MGR_3" & tag_id == "472" & census_date == "2007"),
    !(plot_id == "MGR_3" & tag_id == "472" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "476" & census_date == "2006"),
    !(plot_id == "MGR_3" & tag_id == "476" & census_date == "2007"),
    !(plot_id == "MGR_3" & tag_id == "476" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "562T" & census_date == "2009"),
    !(plot_id == "MGR_3" & tag_id == "654T" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "147N" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "238" & census_date == "2006"),
    !(plot_id == "MGR_4" & tag_id == "238" & census_date == "2007"),
    !(plot_id == "MGR_4" & tag_id == "238" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "268" & census_date == "2006"),
    !(plot_id == "MGR_4" & tag_id == "268" & census_date == "2007"),
    !(plot_id == "MGR_4" & tag_id == "268" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "27N" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "343" & census_date == "2006"),
    !(plot_id == "MGR_4" & tag_id == "343" & census_date == "2007"),
    !(plot_id == "MGR_4" & tag_id == "343" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "418N" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "420N" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "572N" & census_date == "2009"),
    !(plot_id == "MGR_4" & tag_id == "668N" & census_date == "2009"),
    !(plot_id == "MGR_9" & tag_id == "1055-1065" & census_date == "2007"),
    !(plot_id == "MGR_9" & tag_id == "1055-1065" & census_date == "2006"),
    !(plot_id == "MGR_9" & tag_id == "645-655" & census_date == "2007"),
    !(plot_id == "MGR_9" & tag_id == "830-840" & census_date == "2007"),
    !(plot_id == "MGR_9" & tag_id == "855-865" & census_date == "2007"),
    !(plot_id == "MGR_9" & tag_id == "988" & census_date == "2006"),
    !(plot_id == "MGR_9" & tag_id == "988" & census_date == "2007"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2006"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2007"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2008"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2009"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2010"),
    !(plot_id == "MGR_15" & tag_id == "1" & census_date == "2017"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2006"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2007"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2008"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2009"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2010"),
    !(plot_id == "MGR_7" & tag_id == "1" & census_date == "2017"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2006"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2007"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2008"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2009"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2010"),
    !(plot_id == "MGR_8" & tag_id == "1" & census_date == "2017")) %>% 
  mutate(
    # Some trees had multiple species on different stems, make species same.
    species_orig_binom = case_when(
      plot_id == "MGR_14" & tree_id == "28N" ~ "Grewia schinzii",
      plot_id == "MGR_14" & tree_id == "340N" ~ "Diospyros sinensis",
      plot_id == "MGR_5" & tree_id == "239" ~ "Pseudolachnostylis maprouneifolia",
      plot_id == "MGR_5" & tree_id == "405" ~ "Dombeya rotundifolia",
      plot_id == "MGR_5" & tree_id == "407" ~ "Combretum molle",
      plot_id == "MGR_7" & tree_id == "359N" ~ "Bauhinia petersiana",
      plot_id == "MGR_7" & tree_id == "63" ~ "Bauhinia petersiana",
      plot_id == "MGR_7" & tree_id == "298N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_8" & tree_id == "127N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_8" & tree_id == "201N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_7" & tag_id == "226N2" ~ "Brachystegia boehmii",
      plot_id == "MGR_7" & tag_id == "407N2" ~ "Bauhinia petersiana",
      plot_id == "MGR_7" & tag_id == "407" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_7" & tag_id == "418N" ~ "Bauhinia galpinii",
      is.na(species_orig_binom) ~ "Indet indet",
      TRUE ~ species_orig_binom),
    tree_id = case_when(  # Some trees had multiple species on different stems, split tree
      plot_id == "MGR_7" & tag_id == "407N2" ~ tag_id,
      plot_id == "MGR_7" & tag_id == "202N" ~ tag_id,
      plot_id == "MGR_7" & tag_id == "1131N" ~ tag_id,
      plot_id == "MGR_7" & tag_id == "4N" & species_orig_binom == "Pterocarpus rotundifolius" ~ "4NA",
      plot_id == "MGR_6" & tag_id == "68" ~ tag_id,
      plot_id == "MGR_6" & tag_id == "373N3" ~ tag_id,
      plot_id == "MGR_6" & tag_id == "324N2" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "406N2" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "24N2" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "141N3" ~ tag_id,
      plot_id == "MGR_15" & tag_id == "66N3" ~ tag_id,
      plot_id == "MGR_15" & tag_id == "177N2" ~ tag_id,
      plot_id == "MGR_14" & tag_id == "339N2" ~ tag_id,
      TRUE ~ tree_id)) %>% 
	group_by(plot_id, tree_id, census_date) %>% 
	mutate(
    stem_status = case_when(  # Trees can't have dead stems unless all stems dead.
      any(stem_status %in% c("a", "r")) & stem_status == "d" ~ "r",
      TRUE ~ stem_status)) %>% 
  colGen(., stemColClass()) 

# Remove other back-dated measurements not caught by filtering above
stems_2019_split <- split(stems_2019_clean, 
  paste(stems_2019_clean$plot_id, stems_2019_clean$stem_id, sep = "_"))

stems_2019_noback <- do.call(rbind, lapply(stems_2019_split, function(x) {
  first_true <- min(which(!is.na(x$diam) == TRUE))
  if (is.finite(first_true)) {
    x[seq(first_true, nrow(x)),]
  } else {
    x
  }
}))

# Old data to fill gaps 
stems_old_clean <- stems_old %>% 
  filter(  # Some plots weren't included in Thom Brade's cleaned data
    plotcode %in% c("MGR-001", "MGR-002", "MGR-003", "MGR-004", 
      "MGR-009", "MGR-010", "MGR-011", "MGR-012")) %>% 
  dplyr::select(-X, 
    -plot_id,
    plot_id = plotcode, 
    census_date = year, 
    tag_id,
    -stem_id,
    subplot_id = subplot1,
    -subplot2,
    x_grid = x,
    y_grid = y,
    species_orig_local = spp_local,
    species_orig_binom = spp_orig_sci,
    -spp,
    notes_stem = notes_tree, 
    diam, 
    pom,
    -d_pom_tminus1,
    tree_id = multiple,
    -multiple_notes,
    height,
    crown_x = crown_diam,
    stem_status = alive,
    -a_statusF1,
    broken_per_remain = broken_bm_remain,
    broken_height = broken_h,
    stem_mode = death_modeF2,
    diam_method = method_dbhF3,
    -data_mgmtF4,
    height_method = method_hF5,
    -notes_census,
    voucher_id,
    -voucher_collected) %>% 
  mutate(
    stem_id = tag_id,
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    pom = pom / 100,
    fpc = 1,
    species_orig_binom = case_when(
      plot_id == "MGR_10" & tree_id == "554" ~ "Dalbergia melanoxylon",
      plot_id == "MGR_10" & tree_id == "925" ~ "Brachystegia boehmii",
      plot_id == "MGR_11" & tree_id == "448" ~ "Brachystegia boehmii",
      plot_id == "MGR_12" & tree_id == "1" ~ "Diplorhynchus condylocarpon",
      plot_id == "MGR_12" & tree_id == "151-180" ~ "Strychnos henningsii",
      plot_id == "MGR_2" & tree_id == "158" ~ "Cleistanthus schlechateri",
      plot_id == "MGR_2" & tree_id == "168" ~ "Pteleopsis myrtifolia",
      plot_id == "MGR_2" & tree_id == "169" ~ "Cleistanthus schlechateri",
      plot_id == "MGR_2" & tree_id == "16N" ~ "Combretum indet",
      plot_id == "MGR_2" & tree_id == "170" ~ "Strychnos henningsii",
      plot_id == "MGR_2" & tree_id == "171" ~ "Combretum indet",
      plot_id == "MGR_2" & tree_id == "302" ~ "Diplorhynchus condylocarpon",
      plot_id == "MGR_2" & tree_id == "45" ~ "Combretum indet",
      plot_id == "MGR_2" & tree_id == "83" ~ "Crossopteryx febrifuga",
      plot_id == "MGR_2" & tree_id == "84" ~ "Philenoptera violacea",
      plot_id == "MGR_2" & tree_id == "8N" ~ "Stereospermum kunthianum",
      plot_id == "MGR_4" & tree_id == "202N" ~ "Burkea africana",
      plot_id == "MGR_4" & tree_id == "209" ~ "Sclerocarya birrea",
      plot_id == "MGR_4" & tree_id == "227" ~ "Julbernardia globiflora",
      plot_id == "MGR_4" & tree_id == "238" ~ "Erythrophleum africanum",
      plot_id == "MGR_4" & tree_id == "275N" ~ "Lannea discolor",
      plot_id == "MGR_4" & tree_id == "285N" ~ "Julbernardia globiflora",
      plot_id == "MGR_4" & tree_id == "296N" ~ "Cleistanthus schlechateri",
      plot_id == "MGR_4" & tree_id == "375" ~ "Diospyros mespiliformis",
      plot_id == "MGR_4" & tree_id == "387" ~ "Diplorhynchus condylocarpon",
      plot_id == "MGR_4" & tree_id == "390" ~ "Burkea africana",
      plot_id == "MGR_4" & tree_id == "448" ~ "Erythrophleum africanum",
      plot_id == "MGR_4" & tree_id == "448N" ~ "Erythrophleum africanum",
      plot_id == "MGR_4" & tree_id == "509" ~ "Cleistanthus schlechteri",
      plot_id == "MGR_4" & tree_id == "529" ~ "Diplorhynchus condylocarpon",
      plot_id == "MGR_9" & tree_id == "1.0-10" ~ "Hyphaene coriacea",
      is.na(species_orig_binom) ~ "Indet indet",
      TRUE ~ species_orig_binom),
    tree_id = case_when(  # Some trees had multiple species on different stems, split tree
      plot_id == "MGR_9" & tree_id == "830-840" & 
        species_orig_binom == "Philenoptera violacea" ~ "830-840A",
      plot_id == "MGR_9" & tree_id == "650-660" & 
        species_orig_binom == "Philenoptera violacea" ~ "650-660A",
      plot_id == "MGR_9" & tree_id == "581" & 
        species_orig_binom == "Philenoptera violacea" ~ "581A",
      plot_id == "MGR_9" & tree_id == "480" & 
        species_orig_binom == "Acacia sp." ~ "480A",
      plot_id == "MGR_9" & tree_id == "1130-1140" & 
        species_orig_binom == "Philenoptera violacea" ~ "1130-1140A",
      plot_id == "MGR_9" & tree_id == "1130-1140" & 
        species_orig_binom == "Philenoptera violacea" ~ "1130-1140A",
      plot_id == "MGR_9" & tree_id == "1125-1135" & 
        species_orig_binom == "Hyphaene coriacea" ~ "1125-1135A",
      plot_id == "MGR_9" & tree_id == "1125-1135" & 
        species_orig_binom == "Combretum molle or aden or apic" ~ "1125-1135B",
      plot_id == "MGR_9" & tree_id == "1125-1135" & 
        species_orig_binom == "Philenoptera violacea" ~ "1125-1135C",
      plot_id == "MGR_9" & tree_id == "1090-1100" & 
        species_orig_binom == "Dalbergia boehmii" ~ "1090-1100A",
      plot_id == "MGR_9" & tree_id == "108" & 
        species_orig_binom == "Combretum molle or aden or apic" ~ "108A",
      plot_id == "MGR_4" & tree_id == "478" & 
        species_orig_binom == "Combretum molle or aden or apic" ~ "478A",
      plot_id == "MGR_4" & tree_id == "462" & 
        species_orig_binom == "Dalbergia boehmii" ~ "462A",
      plot_id == "MGR_4" & tree_id == "447N" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "447NA",
      plot_id == "MGR_4" & tree_id == "446N" & 
        species_orig_binom == "Cleistanthus schlechateri" ~ "446NA",
      plot_id == "MGR_4" & tree_id == "442N" & 
        species_orig_binom == "Rhoicissus revoilii" ~ "442NA",
      plot_id == "MGR_4" & tree_id == "42N" & 
        species_orig_binom == "Strychnos innocua" ~ "42NA",
      plot_id == "MGR_4" & tree_id == "300N" & 
        species_orig_binom == "Burkea africana" ~ "300NA",
      plot_id == "MGR_4" & tree_id == "288N" & 
        species_orig_binom == "Cleistanthus schlechateri" ~ "288NA",
      plot_id == "MGR_4" & tree_id == "280N" & 
        species_orig_binom == "Cleistanthus schlechateri" ~ "280NA",
      plot_id == "MGR_4" & tree_id == "231" & 
        species_orig_binom == "Lannea discolor" ~ "231A",
      plot_id == "MGR_4" & tree_id == "231" & 
        species_orig_binom == "Lannea discolor" ~ "231A",
      plot_id == "MGR_4" & tree_id == "226" & 
        species_orig_binom == "Lannea discolor" ~ "226A",
      plot_id == "MGR_4" & tree_id == "218N" & 
        species_orig_binom == "Julbernardia globiflora" ~ "218NA",
      plot_id == "MGR_4" & tree_id == "218" & 
        species_orig_binom == "Strychnos innocua" ~ "218A",
      plot_id == "MGR_4" & tree_id == "160" & 
        species_orig_binom == "Monodora stenopetala" ~ "160A",
      plot_id == "MGR_4" & tree_id == "141N" & 
        species_orig_binom == "Lannea discolor" ~ "141NA",
      plot_id == "MGR_3" & tree_id == "703T" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "703TA",
      plot_id == "MGR_3" & tree_id == "646T" & 
        species_orig_binom == "Erythrophleum africanum" ~ "646TA",
      plot_id == "MGR_3" & tree_id == "642T" & 
        species_orig_binom == "Erythrophleum africanum" ~ "642TA",
      plot_id == "MGR_3" & tree_id == "449T" & 
        species_orig_binom == "Pterocarpus angolensis" ~ "449TA",
      plot_id == "MGR_3" & tree_id == "440T" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "440TA",
      plot_id == "MGR_3" & tree_id == "190" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "190A",
      plot_id == "MGR_2" & tree_id == "91-120" & 
        species_orig_binom == "Burkea africana" ~ "91-120A",
      plot_id == "MGR_2" & tree_id == "91-120" & 
        species_orig_binom == "Indet indet" ~ "91-120B",
      plot_id == "MGR_2" & tree_id == "32" & 
        species_orig_binom == "Cleistanthus schlechateri" ~ "32A",
      plot_id == "MGR_2" & tree_id == "301-330" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "301-330A",
      plot_id == "MGR_2" & tree_id == "251" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "251A",
      plot_id == "MGR_2" & tree_id == "190N" & 
        species_orig_binom == "Lannea discolor" ~ "190NA",
      plot_id == "MGR_11" & tree_id == "449" & 
        species_orig_binom == "Diplorhynchus condylocarpon" ~ "449A",
      plot_id == "MGR_11" & tree_id == "258" & 
        species_orig_binom == "Lannea discolor" ~ "258A",
      plot_id == "MGR_10" & tree_id == "928" & 
        species_orig_binom == "unk" ~ "928A",
      plot_id == "MGR_10" & tree_id == "927" & 
        species_orig_binom == "Brachystegia boehmii" ~ "927A",
      plot_id == "MGR_10" & tree_id == "924" & 
        species_orig_binom == "Brachystegia boehmii" ~ "924A",
      plot_id == "MGR_10" & tree_id == "924" & 
        species_orig_binom == "unk" ~ "924B",
      plot_id == "MGR_10" & tree_id == "923" & 
        species_orig_binom == "unk" ~ "923A",
      plot_id == "MGR_10" & tree_id == "923" & 
        grepl("Pterocarpus", species_orig_binom) ~ "923B",
      plot_id == "MGR_10" & tree_id == "60" & 
        grepl("Diplorhynchus", species_orig_binom) ~ "60A",
      plot_id == "MGR_10" & tree_id == "60" & 
        grepl("Brachystegia", species_orig_binom) ~ "60B",
      plot_id == "MGR_1" & tree_id == "348" & 
        grepl("Combretum", species_orig_binom) ~ "348A",
      plot_id == "MGR_10" & tree_id == "343" & 
        grepl("Diplorhynchus", species_orig_binom) ~ "343A",
      plot_id == "MGR_10" & tree_id == "343" & 
        grepl("Diplorhynchus", species_orig_binom) ~ "343A",
      plot_id == "MGR_7" & tree_id == "4N" & 
        species_orig_binom == "Pterocarpus rotundifolius" ~ "4NA",
      TRUE ~ tree_id),
    species_orig_binom = case_when(
      tree_id == "121-150A" ~ "Lannea indet",
      tree_id == "121-150B" ~ "Terminalia brachystemma",
      TRUE ~ species_orig_binom)) %>% 
  filter(
    !grepl("NaN", tag_id),
    !(tag_id == "668N" & plot_id == "MGR_4"),
    !(tag_id == "668N" & plot_id == "MGR_4")
    ) %>% 
	group_by(plot_id, tree_id, census_date) %>% 
	mutate(
    stem_status = case_when(  # Trees can't have dead stems unless all stems dead.
      any(stem_status %in% c("a", "r")) & stem_status == "d" ~ "r",
      TRUE ~ stem_status)) %>% 
  colGen(., stemColClass()) 

# 2021 stems data
stems_2021_matched_clean <- stems_2021_matched %>%
  dplyr::select(
    x_grid,
    y_grid,
    tag_id, 
    tree_id = base_tag_id,
    census_date = measurement_date,
    species_orig_binom,
    pom, 
    diam, 
    stem_status,
    stem_mode,
    decay,
    notes_stem = notes, 
    plot_id, 
    broken_height, 
    damage_cause,
    wound_area) %>% 
  mutate(
    tag_id = case_when(
      tag_id == "4N" & species_orig_binom == "Pterocarpus rotundifolius" ~ "4NA",
      TRUE ~ tag_id),
    tree_id = case_when(  # Split some trees where species name is different
      plot_id == "MGR_5" & tag_id == "217" ~ "217",
      plot_id == "MGR_15" & tag_id == "177N2" ~ tag_id,
      plot_id == "MGR_15" & tag_id == "66N3" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "141N3" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "406N2" ~ tag_id,
      plot_id == "MGR_6" & tag_id == "324N2" ~ tag_id,
      plot_id == "MGR_6" & tag_id == "67" ~ tag_id,
      plot_id == "MGR_5" & tag_id == "24N2" ~ tag_id,
      plot_id == "MGR_7" & tag_id == "1131N" ~ tag_id,
      plot_id == "MGR_8" & tag_id == "17" ~ tag_id,
      plot_id == "MGR_8" & tag_id == "17N4" ~ "17N3",
      plot_id == "MGR_14" & tag_id == "339N2" ~ tag_id,
      plot_id == "MGR_7" & tag_id %in% c("456N", "4NA") & 
        species_orig_binom == "Pterocarpus rotundifolius" ~ "4NA",
      plot_id == "MGR_6" & tag_id == "373N3" ~ tag_id,
      plot_id == "MGR_7" & tag_id == "407N2" ~ tag_id,
      TRUE ~ tree_id),
    species_orig_binom = case_when(  # Some tags need species ID updating
      plot_id == "MGR_14" & tree_id == "31N" ~ "Grewia schinzii",
      plot_id == "MGR_5" & tree_id == "239" ~ "Pseudolachnostylis maprouneifolia",
      plot_id == "MGR_8" & tree_id == "201N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_8" & tree_id == "127N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_7" & tree_id == "63" ~ "Bauhinia petersiana",
      plot_id == "MGR_14" & tree_id == "340" ~ "Diospyros sinensis",
      plot_id == "MGR_7" & tree_id == "298N" ~ "Pterocarpus rotundifolius",
      plot_id == "MGR_7" & tree_id == "359N" ~ "Bauhinia petersiana",
      plot_id == "MGR_5" & tag_id == "405" ~ "Dombeya rotundifolia",
      plot_id == "MGR_5" & tag_id == "407N3" ~ "Combretum molle",
      plot_id == "MGR_14" & tag_id == "99N" & is.na(species_orig_binom) ~ "Cola mossambicensis",
      plot_id == "MGR_5" & tag_id == "28" & is.na(species_orig_binom) ~ "Holarrhena pubescens",
      plot_id == "MGR_7" & tag_id == "407" ~ "Pterocarpus rotundifolius",
      TRUE ~ species_orig_binom),
    fpc = 1,
    stem_id = tag_id,
    pom = case_when(
      broken_height < pom ~ broken_height,
      TRUE ~ as.numeric(pom)),
    stem_status = case_when(
      stem_status == "F" ~ "a",
      TRUE ~ tolower(stem_status)),
    wound_area = ifelse(!is.na(wound_area) | wound_area > 0, "y", NA_character_),
    stem_mode = case_when(
      stem_mode == "D" ~ NA_character_,
      TRUE ~ tolower(stem_mode)),
    stem_mode = pasteVals(stem_mode, wound_area),
    damage_cause = case_when(
      damage_cause %in% c("B", "T", "V") ~ NA_character_,
      TRUE ~ tolower(gsub(",", "", damage_cause))),
    diam_method = "dbh_tape",
    decay = as.numeric(decay)) %>% 
  dplyr::select(-wound_area) %>% 
  colGen(stemColClass())

stems_2021_recruit_clean <- stems_2021_recruit %>% 
  dplyr::select(
    plot_id, 
    x_grid,
    y_grid, 
    tag_id,
    tree_id = base_tag_id,
    recruit,
    census_date = measurement_date,
    species_orig_binom,
    species_orig_local = species_local,
    pom,
    diam,
    stem_status,
    stem_mode, 
    damage_cause,
    decay,
    broken_height,
    notes_stem = notes,
    wounded_binary) %>% 
  mutate(
    recruit = ifelse(recruit == "yes", "n", NA_character_),
    wounded_binary = ifelse(wounded_binary == TRUE, "y", NA_character_),
    pom = case_when(
      pom == 0.3 ~ 1.3,
      TRUE ~ pom),
    stem_mode = tolower(pasteVals(stem_mode, recruit, wounded_binary)),
    damage_cause = tolower(damage_cause),
    tag_id = case_when(  # Some tag IDs are duplicated, add an "N"
      plot_id == "MGR_13" & tag_id == "69P1" & diam == 6.0 ~ paste0(tag_id, "N"),
      plot_id == "MGR_15" & tag_id == "116P1" & species_orig_binom == "Diplorhynchus condylocarpon" ~ paste0(tag_id, "N"),
      plot_id == "MGR_5" & tag_id == "121P1" & species_orig_binom == "Combretum apiculatum" ~ paste0(tag_id, "N"),
      plot_id == "MGR_6" & tag_id == "349P2" & species_orig_binom == "Brachystegia boehmii" ~ paste0(tag_id, "N"),
      plot_id == "MGR_7" & tag_id == "275P1" & x_grid == 95.2 ~ paste0(tag_id, "N"),
      TRUE ~ tag_id),
    tree_id = case_when(  # Some trees have multiple species, split trees by species ID and XY grid coords
      plot_id == "MGR_13" & tag_id == "116P1N" ~ tag_id, 
      plot_id == "MGR_5" & tag_id == "121P1N" ~ tag_id, 
      plot_id == "MGR_6" & tag_id == "349P2N" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "257P2" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "257P2" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "377P3" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "377P4" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "377P5" ~ tag_id, 
      plot_id == "MGR_7" & tag_id == "377P6" ~ "377P5", 
      plot_id == "MGR_7" & tag_id == "37P2" ~ tag_id, 
      plot_id == "MGR_15" & tag_id == "116P1N" ~ tag_id, 
      TRUE ~ tree_id),
    stem_id = tag_id) %>% 
  dplyr::select(-recruit, -wounded_binary) %>% 
  colGen(stemColClass())

# Unmatched stems
stems_2021_nomatch_clean <- stems_2021_nomatch %>% 
  dplyr::select(
    plot_id,
    tag_id, 
    x_grid,
    y_grid,
    tree_id = base_tag_id,
    census_date = measurement_date,
    species_orig_binom,
    pom,
    diam, 
    stem_status,
    stem_mode,
    damage_cause,
    decay,
    notes_stem = notes,
    wound_area = wound_size_x,
    species_orig_local) %>% 
  mutate(
    decay = as.numeric(decay),
    pom = as.numeric(pom),
    stem_id = tag_id,
    stem_status = tolower(stem_status),
    stem_mode = tolower(stem_mode),
    damage_cause = tolower(damage_cause)) %>% 
  colGen(stemColClass())

# Join stems tables
stems_join <- bind_rows(list(stems_2019_noback, stems_old_clean, stems_2021_matched_clean, stems_2021_recruit_clean, stems_2021_nomatch_clean)) 

stems_join_clean <- stems_join %>% 
  filter(!is.na(plot_id))

# Check stems table
stems_check_list <- stemTableCheck(stems_join_clean)
stems_check <- stems_check_list[[1]]
stems_flags <- stems_check_list[[2]]

# Check species
stems_species <- speciesCheck(
  stems_check$species_orig_binom, 
  ret_unk_seosaw = TRUE, ret_unk_gnr = FALSE, ret_unk_cjb = FALSE)

lookup <- data.frame(
  original = stems_species,
  corrected = case_when(
    stems_species == "Acacia nigrescens or polya or sieber" ~ "Senegalia indet",
    stems_species == "Cleistanthus schlechateri" ~ "Cleistanthus schlechteri",
    stems_species == "Combretum molle or aden or apic" ~ "Combretum indet",
    stems_species == "Faidherbia albida or A. robusta" ~ "Fabaceae indet",
    stems_species == "Khaya anthoteca" ~ "Khaya anthotheca",
    stems_species == "Pterocarpus rotundfolius spp polyanthus or spp rotundfolius or Pterocarpus brenanii" ~ "Pterocarpus rotundifolius",
    stems_species == "Serisia chirindensis" ~ "Searsia chirindensis",
    stems_species == "Terminalia stenostachya or brachystemma" ~ "Terminalia indet",
    stems_species == "unk" ~ "Indet indet",
    stems_species == "Bauhinia galpinni" ~ "Bauhinia galpinii",
    stems_species == "Senegalia galpinni" ~ "Senegalia galpinii",
    stems_species == "Bauhinia galpinni" ~ "Bauhinia galpinii",
    stems_species == "Dalbergia bochmii" ~ "Dalbergia boehmii",
    stems_species == "Dalbergia nyasae" ~ "Dalbergiella nyasae",
    stems_species == "Diospyris sinensis" ~ "Diospyros cathayensis",
    stems_species == "Scolopia stolzi" ~ "Scolopia stolzii",
    stems_species == "Ziziphus mucramata" ~ "Ziziphus mucronata",
    TRUE ~ NA_character_))
    
stems_species <- cbind(stems_check,
  speciesCheck(stems_check$species_orig_binom, lookup_user = lookup,
    ret_unk_seosaw = FALSE, ret_unk_gnr = FALSE, ret_unk_cjb = FALSE))

# Add extra columns
stems_wd <- cbind(stems_species, woodDensity(stems_species))
stems_wd$agb <- chave14(stems_wd, plots_check)
stems_wd$subdiv_id <- NA_character_

# Check the stem and plot tables together
stems_plots_flags <- stemPlotTableCheck(stems_wd, plots_check)

# Generate plot level statistics from stem table
plots_stat <- cbind(plots_check, 
  censusStemColGen(stems_wd, plots_check))

# Export data
exportData(file.path(cln_dir, "MGR"), 
  stems = stems_wd, plots = plots_stat, polys = polys_clean,
  stems_flags = stems_flags, 
  plots_flags = plots_flags,
  stems_plots_flags = stems_plots_flags)

