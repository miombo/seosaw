# Clean Zimbabwe ZHF data
# John Godlee (johngodlee@gmail.com)
# Last updated: 2022-11-12

##' Hurungwe
##' one-off plots  in Karoi district also known as Hurungwe
##' 15 m diameter circles
##' Surveyed in 2014

# Packages
library(dplyr)
library(seosawr)

# Import data
stems <- read.csv(file.path(raw_dir, "ZHF/18_zimbabwe_hurungwe_zfc_clean.csv"))
plots <- read.csv(file.path(raw_dir, "ZHF/hurungwe_plots.csv"))

# Clean plots
plots_clean <- plots %>% 
  dplyr::select(
    plot_id = PlotCode,
    plot_name = PlotName,
    census_date = all.dates,
    -n.censuses,
    prinv = PI,
    plot_shape = Shape.of.plot,
    plot_radius = Plot.Diameter,
    nested_1 = Nested.,
    min_diam_thresh = N1..Min.tree.Diameter,
    lianas_sampled = Lianas.sampled,
    soil_sampled = Soil.sampled,
    ground_layer_sampled = Understory.sampled,
    stumps_sampled = Stumps.sampled,
    land_use_notes = Land.use.history.timeline,
    longitude_of_centre = Longitude.of.centre,
    latitude_of_centre = Latitude.of.centre,
    polygons = Polygon.data.exist.,
    manipulation_experiment = Manipulation.experiment.,
    local_community_rights = Local.community.rights,
    timber_harvesting = Timber.harvesting.occurring,
    fuel_wood_harvesting = Fuel.wood.harvesting,
    charcoal_harvesting = Charcoal.harvesting,
    other_woody_product_harvesting = Other.woody.product.harvesting,
    ntfp_harvesting = NTFP.harvesting,
    farmed_30_years = Used.for.farming.in.last.30.years.,
    fire_return_interval = Treatment.fire.return.interval,
    fire_exclusion = Protected.from.fire,
    exclosure = Elephant.exclosure,
    elephants = Elephants,
    elephant_density = Elephant.density,
    goat_grazing = Goats.graze.here,
    cattle_grazing = Cattle.graze.here) %>% 
  mutate(
    land_use_notes = pasteVals(land_use_notes, timber_harvesting, 
      fuel_wood_harvesting, farmed_30_years, ntfp_harvesting,
      sep = "; "),
    fire_notes = fire_return_interval,
    fire_return_interval = 1,
    timber_harvesting = FALSE,
    fuel_wood_harvesting = TRUE,
    charcoal_harvesting = FALSE,
    other_woody_product_harvesting = TRUE,
    ntfp_harvesting = TRUE,
    fire_exclusion = FALSE,
    exclosure = NA_character_,
    elephant_density = NA_character_,
    goat_grazing = TRUE,
    cattle_grazing = TRUE,
    soil_sampled = FALSE,
    ground_layer_sampled = FALSE,
    stumps_sampled = FALSE,
    manipulation_experiment = FALSE,
    meas_tree_stem = "tree",
    polygons = TRUE,
    tag_tree_stem = NA_character_,
    tree_diff = FALSE,
    dataset_id = "ZHF",
    country_iso3 = "ZWE",
    other_authors = "Muchawona A.",
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    plot_cluster = plot_id,
    permanent = FALSE,
    tags = FALSE,
    nested_1 = FALSE,
    nested_2 = FALSE,
    nested_3 = FALSE,
    lianas_sampled = NA,
    coarse_woody_debris_sampled = FALSE,
    small_stems_sampled = FALSE,
    dead_stems_sampled = FALSE,
    abandoned = NA,
    stem_timeline_cont = NA,
    subdiv = FALSE,
    date_metadata = 2020,
    file_stems = "18_zimbabwe_hurungwe_zfc_clean.csv",
    file_plots = "hurungwe_plots.csv",
    pom_default = 1.3) %>% 
  colGen(., plotColClass())

plots_check_list <- plotTableCheck(plots_clean)
plots_check <- plots_check_list[[1]]
plots_flags <- plots_check_list[[2]]

# Polygons
polys <- polyEstGen(plots_check)

# Clean stems
stems_clean <- stems %>%
  dplyr::select(
    -X, 
    -plot_id,
    plot_id = plotcode, 
    census_date = year,
    tag_id,
    stem_id,
    -stem_id.1,
    subplot_id = subplot1,
    -subplot2,
    -x,
    -y,
    species_orig_local = spp_local,
    species_orig_binom = spp_orig_sci,
    -spp,
    notes_stem = notes_tree,
    diam, 
    pom,
    -d_pom_tminus1,
    -multiple,
    -multiple_notes,
    height,
    crown_x = crown_diam,
    stem_status = alive,
    -a_statusF1,
    broken_per_remain = broken_bm_remain,
    broken_height = broken_h,
    -death_modeF2,
    diam_method = method_dbhF3,
    -data_mgmtF4,
    height_method = method_hF5,
    -notes_census,
    voucher_id,
    -voucher_collected
    ) %>%
  mutate(
    tree_id = stem_id,
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    pom = pom / 100,
    fpc = 1,
    height = case_when(
      TRUE ~ height)
  ) %>%
  colGen(., stemColClass()) 

stems_check_list <- stemTableCheck(stems_clean)
stems_check <- stems_check_list[[1]]
stems_flags <- stems_check_list[[2]]

# Check species names
stems_species <- speciesCheck(stems_check$species_orig_binom, 
  ret_unk_seosaw = TRUE, ret_unk_gnr = TRUE, ret_unk_cjb = TRUE)

lookup <- data.frame(
  original = stems_species,
  corrected = case_when(
    stems_species == "Bauhenea petersiana" ~ "Bauhinia petersiana",
    stems_species == "Brachystegia bohemii" ~ "Brachystegia boehmii",
    stems_species == "Julbernadia globiflora" ~ "Julbernardia globiflora",
    stems_species == "other" ~ "Indet indet",
    stems_species == "Other" ~ "Indet indet",
    TRUE ~ NA_character_))

stems_species <- cbind(stems_check, 
  speciesCheck(stems_check$species_orig_binom, lookup_user = lookup, 
    ret_unk_seosaw = FALSE, ret_unk_gnr = FALSE, ret_unk_cjb = FALSE))

# Generate extra columns related 
stems_wd <- cbind(stems_species, woodDensity(stems_species))
stems_wd$agb <- chave14(stems_wd, plots_check)
stems_wd$subdiv_id <- NA_character_

# Check the stem and plot tables together
stems_plots_flags <- stemPlotTableCheck(stems_wd, plots_check)

# Generate plot level statistics from stem table 
plots_stat <- cbind(plots_check, 
  censusStemColGen(stems_wd, plots_check))

# Export data
exportData(file.path(cln_dir, "ZHF"), 
  stems = stems_wd, plots = plots_stat, polys = polys, 
  stems_flags = stems_flags,
  plots_flags = plots_flags,
  stems_plots_flags = stems_plots_flags)


