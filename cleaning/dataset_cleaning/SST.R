# Clean Satara data
# Penny Mograbi, Tatenda Gotore, John Godlee (johngodlee@gmail.com)
# Last updated: 2024-06-27


# Packages
library(dplyr)
library(seosawr)
library(sf)
library(stringr)

# Import 2018 data
stems_18 <- read.csv(file.path(raw_dir, "SST", "satara_stems_master.csv"))
plots_18 <- read.csv(file.path(raw_dir, "SST", "satara_plots.csv"), sep = ";")
polys_18 <- st_read(file.path(raw_dir, "SST", "plot_lines/plot_lines.shp"))

# Import 2023 data
stems_1 <- read.csv(file.path(raw_dir, "SST", "SSA_1_stems 1_2023.csv"))
stems_2 <- read.csv(file.path(raw_dir, "SST", "SSA_2_stems 1_2023.csv"))
stems_3 <- read.csv(file.path(raw_dir, "SST", "SSA_3_stems 1_2023.csv"))
plots_23 <- read.csv(file.path(raw_dir, "SST", "SAA_plots_clean_2023.csv"))
polys_23 <- read_sf(file.path(raw_dir, "SST", "SST_polys_2023.shp"))

# Clean 2018 plots data
plots_clean_18 <- plots_18 %>% 
  dplyr::select(
    plot_name = Plot.name,
    -PI,
    -Country,
    -Site,
    -Long,
    -Lat,
    plot_shape = Shape,
    plot_length = plot_lenght,
    plot_width = plot_width) %>% 
  mutate(
    dataset_id = "SST",
    plot_id = paste(dataset_id, row_number(), sep = "_"),
    plot_cluster = plot_id,
    census_date = "2018",
    coarse_woody_debris_sampled = FALSE,
    small_stems_sampled = FALSE,
    soil_sampled = FALSE,
    lianas_sampled = FALSE,
    ground_layer_sampled = FALSE,
    dead_stems_sampled = FALSE,
    meas_tree_stem = "stem",
    tag_tree_stem = "stem",
    tree_diff = FALSE,
    tags = TRUE,
    permanent = TRUE,
    date_metadata = 2018,
    country_iso3 = "ZAF",
    prinv = "Sally Archibald",
    min_diam_thresh = NA_real_,
    min_height_thresh = NA_real_,
    nested_1 = FALSE,
    nested_2 = FALSE,
    nested_3 = FALSE,
    fire_treatment = TRUE,
    fire_return_interval = 1,
    fire_treatment_season = case_when(
      plot_name == "E5_4" ~ "early_dry_season",
      plot_name == "L5_2" ~ "late_dry_season",
      plot_name == "L5_3" ~ "late_dry_season",
      TRUE ~ NA_character_),
    fire_notes = "Burned in 2013, 2014, 2015, 2017, and 2019. Fire treatment ended in 2019",
    plot_length = 223.61, 
    plot_width = 223.61,
    latitude_of_centre = case_when(
      plot_name == "E5_4" ~ -24.407886,
      plot_name == "L5_2" ~ -24.407311,
      plot_name == "L5_3" ~ -24.420826,
      TRUE ~ NA_real_),
    longitude_of_centre = case_when(
      plot_name == "E5_4" ~ 31.786724,
      plot_name == "L5_2" ~ 31.799695,
      plot_name == "L5_3" ~ 31.901897,
      TRUE ~ NA_real_),
    subdiv = TRUE,
    xy_method = "gps",
    elephants = TRUE,
    experiment_ref = "https://doi.org/10.1111/1365-2664.12956",
    pa = TRUE,
    pa_name = "Kruger National Park",
    pa_type = "National Park",
    wdpa_id = 873,
    file_plots = "satara_plots.csv",
    elephant_density = 1,
    elephant_data_source = "https://doi.org/10.4102/koedoe.v63i1.1660",
    exclosure = NA,
    catenal_position = "flat",
    small_nonsocial_browsers = NA,
    large_browsers = TRUE,
    medium_sized_social_mixed_diet = NA,
    water_dependent_grazers = TRUE,
    nonruminants_excluding_suids = NA,
    goat_grazing = FALSE,
    cattle_grazing = FALSE,
    herbivore_notes = "Wildebeest, Impala, Giraffe, Kudu, Buffalo, Elephant, Rhino, Zebra, Duiker",
    min_diam_thresh = 5,
    file_stems = "stems_clean.csv",
    plot_notes = case_when(
      plot_id == "SST_1" ~ 
        "2023 census: subdivided into 4 x 1 ha plots: SST_11, SST_12, SST_13, SST_14",
      plot_id == "SST_2" ~ 
        "2023 census: subdivided into 4 x 1 ha plots: SST_21, SST_22, SST_23, SST_24",
      plot_id == "SST_3" ~ 
        "2023 census: subdivided into 4 x 1 ha plots: SST_31, SST_32, SST_33, SST_34",
      TRUE ~ NA_character_),
    pom_default = 1.3) %>%
  mutate(
    plot_notes = paste(plot_notes, "2016 (no fire treatment due to drought, lack of grass); 2020 lidar data available", sep = ";")) %>%
  colGen(., plotColClass())

plots_check_list_18 <- plotTableCheck(plots_clean_18)
plots_check_18 <- plots_check_list_18[[1]]
plots_flags_18 <- plots_check_list_18[[2]]

# Clean polygons
polys_clean_18 <- polys_18 %>% filter(
  Name %in% c("Early 5-4", "Late 5-2", "Late 5-3")) %>% 
  st_convex_hull() %>% 
  mutate(plot_id = case_when(
    Name == "Late 5-3" ~ "SST_1",
    Name == "Late 5-2" ~ "SST_2",
    Name == "Early 5-4" ~ "SST_3",
    TRUE ~ NA_character_)) %>% 
  dplyr::select(plot_id)

polyPlotCheck(plots_check_18, polys_clean_18)

# Clean 2023 plots data

# Get plot centres
centres_23 <- polys_23 %>%
  st_centroid() %>%
  rename(plot_id = subdiv_id) %>%
  bind_cols(., st_coordinates(.)) %>%
  rename(
    longitude_of_centre = X,
    latitude_of_centre = Y) %>%
  st_drop_geometry()

# clean polys
polys_clean_23 <- polys_23 %>%
  rename(plot_id = subdiv_id)

# Plots cleaning
plots_clean_val_23 <- plots_23 %>%
  rename(
    plot_cluster = Plot.cluster,
    prinv = Principal.Investigator.name,
    plot_shape = Plot.shape,
    plot_length = Plot.length,
    plot_width = Plot.width,
    plot_slope = Plot.slope,
    plot_aspect = Plot.aspect,
    slope_method = Slope.method,
    catenal_position = Catenal.position,
    min_diam_thresh = Minimum.diameter.threshold,
    min_height_thresh = Minimum.height.threshold,
    plot_notes = Miscellaneous.plot.notes,
    termites = Number.of.termite.mounds,
    nested = How.many.levels.of.nesting.are.there.,
    tags = Tags,
    lianas_sampled = Lianas,
    soil_sampled = Soil,
    ground_layer_sampled = Herbaceous,
    stumps_sampled = Tree.stumps,
    coarse_woody_debris_sampled = Coarse.woody.debris,
    small_stems_sampled = Small.stems,
    longitude_of_centre = longitude,
    latitude_of_centre = latitude,
    abandoned = Plot.abandoned,
    stem_timeline_cont = Stem.timeline.continuity,
    permanent = Permanent.plot,
    sampling_notes = Sampling.notes,
    sampling_design_code = Sampling.design.code,
    country_iso3 = Country,
    manipulation_experiment = Manipulation.experiment,
    manipulation_experiment_notes = Describe.the.manipulation.experiment,
    pa = Is.the.plot.in.a.protected.area.,
    pa_name = Name.of.protected.area,
    pa_type = Type.of.protected.area,
    local_community_rights = Local.community.rights,
    other_user_rights = Other.user.rights,
    timber_harvesting = Timber.harvesting,
    high_graded_100_years = High.graded.in.last.100.years,
    clear_cut = Clear.cut,
    fuel_wood_harvesting = Fuel.wood.collection,
    charcoal_harvesting = Wood.collection.for.charcoal,
    other_woody_product_harvesting = Other.woody.products,
    ntfp_harvesting = Non.woody.products,
    farmed_30_years = Farmed.in.last.30.years,
    mining = Mining,
    meas_tree_stem = Trees.or.stems.measured,
    tag_tree_stem = Trees.or.stems.tagged,
    fire_return_interval = Fire.return.interval,
    fire_last_date = Date.of.last.fire..approx..,
    fire_exclusion = Is.there.a.fire.exclusion.experiment.in.this.plot.,
    fire_exclusion_success = 
      To.what.extent.were.fires.successfully.excluded.from.the.plot.,
    fire_treatment = Is.there.a.fire.treatment.experiment.in.this.plot.,
    fire_treatment_season = The.season.in.which.fires.are.set,
    fire_cause = What.commonly.causes.fires.in.the.plot.,
    fire_fuel = What.do.fires.on.this.plot.typically.burn.,
    fire_notes = Fire.notes,
    drought = Drought,
    drought_last_date = Date.of.last.drought..approx..,
    wind = Wind,
    wind_last_date = Date.of.last.wind.storm..approx..,
    earthquake = Earthquake,
    earthquake_last_date = Date.of.last.earthquake..approx..,
    landslide = Landslide,
    landslide_last_date = Date.of.last.landslide..approx..,
    flood = Flooding,
    flood_last_date = Date.of.last.flood..approx..,
    cyclone = Cyclone,
    elephants = Elephants,
    elephant_density = Elephant.density,
    elephant_data_source = Source.of.elephant.density,
    small_nonsocial_browsers = Small.non.social.browsers,
    large_browsers = Large.browsers,
    medium_sized_social_mixed_diet = Medium.sized.social.mixed.diet,
    nonruminants_excluding_suids = Non.ruminants..excluding.suids,
    water_dependent_grazers = Water.dependent.grazers,
    goat_grazing = Goats,
    cattle_grazing = Cattle,
    herbivory_notes = Herbivory.notes,
    photos = img_file) %>%
  mutate(
    dataset_id = "SST",
    date_metadata = 2023,
    census_date = "2023-05",
    plot_id = case_when(
      plot_name == "SSA_1_E54_1" ~ "SST_31",
      plot_name == "SSA _1_E5_4_2" ~ "SST_32",
      plot_name == "SSA_1_E5_4_3" ~ "SST_33",
      plot_name == "SSA _1_E5_4_4" ~ "SST_34",
      plot_name == "SSA_2_L52_1" ~ "SST_21",
      plot_name == "SSA_2_L52_2" ~ "SST_22",
      plot_name == "SSA_2_L52_3" ~ "SST_23",
      plot_name == "SSA_2_L52_4" ~ "SST_24",
      plot_name == "SSA_3_L5_3_1" ~ "SST_11",
      plot_name == "SSA_3_L5_3_2" ~ "SST_12",
      plot_name == "SSA_3_L5_3_3" ~ "SST_13",
      plot_name == "SSA_3_L5_3_4" ~ "SST_14"),
    plot_cluster = case_when(
      plot_id == "SST_31" ~ "SST_3",
      plot_id == "SST_32" ~ "SST_3",
      plot_id == "SST_33" ~ "SST_3",
      plot_id == "SST_34" ~ "SST_3",
      plot_id == "SST_21" ~ "SST_2",
      plot_id == "SST_22" ~ "SST_2",
      plot_id == "SST_23" ~ "SST_2",
      plot_id == "SST_24" ~ "SST_2",
      plot_id == "SST_11" ~ "SST_1",
      plot_id == "SST_12" ~ "SST_1",
      plot_id == "SST_13" ~ "SST_1",
      plot_id == "SST_14" ~ "SST_1"),
    catenal_position = case_when(catenal_position == "Flat" ~ "flat"),
    meas_tree_stem = case_when(meas_tree_stem == "Stem" ~ "stem"),
    tag_tree_stem = case_when(tag_tree_stem == "Stem" ~ "stem"),
    fire_treatment_season = case_when(
      fire_treatment_season == "Late dry season" ~ "late_dry_season",
      fire_treatment_season == "Early dry season" ~ "early_dry_season"),
    nested_1 = case_when(
      nested %in% c("1", "2", "3") ~ TRUE,
      TRUE ~ FALSE),
    nested_2 = case_when(
      nested %in% c("2", "3") ~ TRUE,
      TRUE ~ FALSE),
    nested_3 = case_when(
      nested %in% c("3") ~ TRUE,
      TRUE ~ FALSE),
    file_plots = "SAA_plots_clean.csv",
    file_stems = "SSA_1_stems 1.csv; SSA_2_stems 1.csv; SSA_3_stems 1.csv",
    subdiv = FALSE) %>%
  select(
    -c(
      Data.collector.name,
      Plot.diameter, Describe.the.irregular.plot.shape,
      plot_slope, plot_aspect, Describe.slope.method,
      Are.there.subplots., How.many.subplots.,
      Subplot.shape, Subplot.length, Subplot.width,
      Subplot.diameter,
      nested,
      N1.minimum.diameter.threshold,
      N1.minimum.height.threshold, N1.Plot.shape,
      N1.Plot.length, N1.Plot.width, N1.Plot.diameter,
      N2.minimum.diameter.threshold,
      N2.minimum.height.threshold, N2.Plot.shape,
      N2.Plot.length, N2.Plot.width, N2.Plot.diameter,
      N3.minimum.diameter.threshold,
      N3.minimum.height.threshold, N3.Plot.shape,
      N3.Plot.length, N3.Plot.width, N3.Plot.diameter,
      Choose.method.of.recording.plot.location,
      Plot.corners, Plot.centre, Longitude, Latitude,
      Which.animals.visit.the.plot.,
      Are.elephants.excluded.from.this.plot.by.humans.,
      Are.browsers.excluded.from.the.plot.by.humans.,
      What.sizes.of.animals.are.excluded.,
      herbivory_notes, What.was.sampled.,
      Dead.stems, Stems.grouped.into.trees,
      What.land.uses.are.seen.in.the.plot.,
      Land.use.notes,
      What.do.fires.on.this.plot.typically.burn..Canopy,
      What.do.fires.on.this.plot.typically.burn..Understorey..grass.and.shrubs.,
      What.do.fires.on.this.plot.typically.burn..Litter,
      What.commonly.causes.fires.in.the.plot..Management,
      What.commonly.causes.fires.in.the.plot..Accidental,
      What.commonly.causes.fires.in.the.plot..Agricultural,
      What.commonly.causes.fires.in.the.plot..Lightning,
      What.commonly.causes.fires.in.the.plot..Unknown,
      What.natural.diasters.occur.in.the.plot.,
      Date.of.last.cyclone..approx.., Plot.name)) %>%
  mutate(
    elephant_data_source = "Local knowledge",
    sampling_design_code = "SEOSAW v3.6",
    pa_name = "Kruger National Park",
    pa_type = "National Park",
    local_community_rights = "Thatch grass harvesting",
    other_user_rights = "Tourism",
    drought_last_date = "2014-12-01",
    prinv = "Sally Archibald",
    other_authors = "Kate Parr, Jason Donaldson, Tercia Strydom, Happy Mangena, Tatenda Gotore",
    dead_stems_sampled = FALSE,
    xy_method = "tape",
    experiment_ref = "https://doi.org/10.1111/1365-2664.12956",
    wdpa_id = "873",
    livestock_30_years = FALSE,
    sheep_grazing = FALSE,
    data_ref = "https://doi.org/10.1111/1365-2664.12956") %>%
  dplyr::select(-latitude_of_centre, -longitude_of_centre) %>%
  mutate(
    manipulation_experiment = case_when(
      plot_name == "SSA_1_E54_1" ~ TRUE,
      TRUE ~ manipulation_experiment),
    manipulation_experiment_notes = case_when(
      plot_name == "SSA_1_E54_1" ~ "Early dry season burning",
      TRUE ~ manipulation_experiment_notes),
    min_height_thresh = NA_character_,
    fire_notes = plot_notes,
    plot_notes = case_when(
      plot_id == "SST_11" ~ "2018 census: subdivision of larger SST_1 plot",
      plot_id == "SST_12" ~ "2018 census: subdivision of larger SST_1 plot",
      plot_id == "SST_13" ~ "2018 census: subdivision of larger SST_1 plot",
      plot_id == "SST_14" ~ "2018 census: subdivision of larger SST_1 plot",
      plot_id == "SST_21" ~ "2018 census: subdivision of larger SST_2 plot",
      plot_id == "SST_22" ~ "2018 census: subdivision of larger SST_2 plot",
      plot_id == "SST_23" ~ "2018 census: subdivision of larger SST_2 plot",
      plot_id == "SST_24" ~ "2018 census: subdivision of larger SST_2 plot",
      plot_id == "SST_31" ~ "2018 census: subdivision of larger SST_3 plot",
      plot_id == "SST_32" ~ "2018 census: subdivision of larger SST_3 plot",
      plot_id == "SST_33" ~ "2018 census: subdivision of larger SST_3 plot",
      plot_id == "SST_34" ~ "2018 census: subdivision of larger SST_3 plot",
      TRUE ~ NA_character_)) %>%
  left_join(., centres_23, by = "plot_id") %>%
  mutate(
    stem_timeline_cont = FALSE) %>%
  colGen(., plotColClass())

plots_check_list_23 <- plotTableCheck(plots_clean_val_23)
plots_check_23 <- plots_check_list_23[[1]]
plots_flags_23 <- plots_check_list_23[[2]]

# Clean 2018 stems data
stems_clean_18 <- stems_18 %>% 
  dplyr::select(
    -plot_id,
    plot_name, 
    species_orig_binom = species,
    height, 
    longitude, 
    latitude,
    diam0.3,
    diam = diam1.3,
    tag_id = tag,
    -Notes,
    -tree_id,
    stem_status = alive,
    -PI,
    -Country,
    -Site,
    census_date = measurement_date) %>% 
  left_join(., plots_check_18[, c("plot_id", "plot_name")], 
    by = "plot_name") %>% 
  filter(is.na(stem_status)) %>% 
  ##' Although three stems were counted as dead, 
  ##' they were not measured or tagged, so are excluded
  mutate(
    species_orig_binom = str_replace_all(species_orig_binom, 
      "^\\u00a0+|\\u00a0+$", ""),
    species_orig_binom = str_replace_all(species_orig_binom, 
      "\uFFFD", ""),
    fpc = 1,
    diam_method = "dbh_tape",
    stem_id = tag_id,
    tree_id = tag_id,
    diam = diam / 10,
    diam0.3 = diam0.3 / 10,
    height = case_when(
      height == 20 ~ 2,
      TRUE ~ height),
    stem_status = "a",
    pom = 1.3,
    notes_stem = paste0("Diameter at 0.3 m: ", diam0.3)) %>% 
  cbind(., gridCoordGen(., polys_clean_18)) %>% 
  dplyr::select(-diam0.3, -plot_name) %>% 
  #the stems below have incorrect coordinates
  mutate(
    across(c(longitude, latitude, x_grid, y_grid), 
                ~ ifelse(tag_id == "AL" & plot_id == "SST_1", NA, .))) %>%
  mutate(
    across(c(longitude, latitude, x_grid, y_grid), 
                ~ ifelse(tag_id == "16AAC" & plot_id == "SST_1", NA, .))) %>%
  mutate(
    across(c(longitude, latitude, x_grid, y_grid), 
                ~ ifelse(tag_id == "AK" & plot_id == "SST_1", NA, .))) %>%
  colGen(., stemColClass()) 

stems_check_list_18 <- stemTableCheck(stems_clean_18)
stems_check_18 <- stems_check_list_18[[1]]
stems_flags_18 <- stems_check_list_18[[2]]

# Check species
stems_species_18 <- speciesCheck(stems_check_18$species_orig_binom, 
  ret_unk_seosaw = TRUE, ret_unk_cjb = TRUE, ret_unk_gnr = TRUE) 

lookup_18 <- data.frame(
  original = stems_species_18,
  corrected = case_when(
    stems_species_18 == "Gymnosporia harveyana" ~ "Gymnosporia harveyana",
    stems_species_18 == "Lannea schweinfurthi" ~ "Lannea schweinfurthii",
    stems_species_18 == "Vachelliatortilis" ~ "Vachellia tortilis",
    TRUE ~ NA_character_))

stems_species_18 <- cbind(stems_check_18, 
  speciesCheck(stems_check_18$species_orig_binom, lookup_user = lookup_18, 
    ret_unk_seosaw = FALSE, ret_unk_cjb = FALSE, ret_unk_gnr = TRUE))

# Add extra columns
stems_wd_18 <- cbind(stems_species_18, woodDensity(stems_species_18))
stems_wd_18$agb <- chave14(stems_wd_18, plots_check_18)
stems_wd_18$subdiv_id <- NA_character_

# Check the stem and plot tables together
stems_plots_flags_18 <- stemPlotTableCheck(stems_wd_18, plots_check_18)

# Generate plot level statistics from stem table
plots_stat_18 <- cbind(plots_check_18, 
  censusStemColGen(stems_wd_18, plots_check_18))

# Clean 2023 stems data
stems_clean_23 <- rbind(stems_1, stems_2, stems_3) %>%
  select(
    -c(X, species_name_sanit, genus_clean, species_clean, confer_clean, 
      subspecies_clean, variety_clean, species_name_clean, species_name_gnr, 
      taxon_rank_gnr, taxon_gnr, base_rank_gnr, 
      family_gnr, species_name_cjb)) %>%
  mutate(
    diam_method = "dbh_tape",
    height_method = "other") %>%
  colGen(., stemColClass()) %>%
  # x_grid & y_grid recorded per tree. Fill stem xy values with tree xy values
  group_by(plot_id, tree_id) %>%
  mutate(
    x_grid = if_else(is.na(x_grid), first(x_grid[!is.na(x_grid)]), x_grid), 
    y_grid = if_else(is.na(y_grid), first(y_grid[!is.na(y_grid)]), y_grid)) %>%
  ungroup() %>%
  filter(tag_id != "SST_23_107") %>%  # no diam
  mutate(
    tag_id = str_replace(tag_id, ".*_", ""),
    census_date = "2023-05") %>%
  #remove dummy stem entries
  filter(!(tag_id %in% c("Hhh", "Ttt", "Ggg"))) %>%
  mutate(
    next_census_check = ifelse(stem_id == "SST_14_363",paste("confirm diam & height"), next_census_check))

stems_check_list_23 <- stemTableCheck(stems_clean_23)
stems_check_23 <- stems_check_list_23[[1]]
stems_flags_23 <- stems_check_list_23[[2]]

# Check species names
stems_species_23 <- speciesCheck(stems_check_23$species_orig_binom,
  ret_unk_seosaw = TRUE, ret_unk_gnr = TRUE, ret_unk_cjb = TRUE)

lookup_23 <- data.frame(
  original = stems_species_23,
  corrected = case_when(
    stems_species_23 == "Sclerocarea birrea" ~ "Sclerocarya birrea",
    stems_species_23 == "Terminalia pronoides" ~ "Terminalia prunioides",
    stems_species_23 == "Other" ~ "Indet indet",
    stems_species_23 == "Philtenoptera violacea" ~ "Philenoptera violacea",
    stems_species_23 == "Ziziphous macronata" ~ "Ziziphus mucronata",
    stems_species_23 == "Acacia tortillas" ~ "Vachellia tortilis",
    TRUE ~ NA_character_))

stems_species_23 <- cbind(stems_check_23, 
  speciesCheck(stems_check_23$species_orig_binom, lookup_user = lookup_23, 
    ret_unk_seosaw = FALSE, ret_unk_cjb = FALSE, ret_unk_gnr = FALSE))

# Generate extra columns related
stems_wd_23 <- cbind(stems_species_23, woodDensity(stems_species_23))
stems_wd_23$agb <- chave14(stems_wd_23, plots_check_23)
stems_wd_23$subdiv_id <- NA_character_

# Check the stem and plot tables together
stems_plots_flags_23 <- stemPlotTableCheck(stems_wd_23, plots_check_23)

# Generate plot level statistics from stem table
plots_stat_23 <- cbind(plots_check_23,
  censusStemColGen(stems_wd_23, plots_check_23))

# join 2018 & 2023 data
polys_clean <- rbind(polys_clean_18, polys_clean_23)
plots_stat <- rbind(plots_stat_18, plots_stat_23) %>%
  #removed month so that seosawr::exportData writes
  mutate(census_date = if_else(census_date == "2023-05", "2023", census_date))
stems_wd <- rbind(stems_wd_18, stems_wd_23) %>%
  #removed month so that seosawr::exportData writes
  mutate(census_date = if_else(census_date == "2023-05", "2023", census_date))
stems_flags <- c(stems_flags_18, stems_flags_23)
plots_flags <- c(plots_flags_18, plots_flags_23)
stems_plots_flags <- c(stems_plots_flags_18, stems_plots_flags_23)

# Export data
exportData(file.path(cln_dir, "SST"), 
  stems = stems_wd, plots = plots_stat, polys = polys_clean, 
  stems_flags = stems_flags,
  plots_flags = plots_flags,
  stems_plots_flags = stems_plots_flags)

